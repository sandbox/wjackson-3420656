<?php

/**
 * @file
 * Primary module hooks for Audit Export Core module.
 */

use Drupal\Core\Database\Database;

/**
 * Implements hook_cron().
 */
function audit_export_core_cron() {
  // Get configuration settings.
  $config = \Drupal::config('audit_export_core.settings');
  $cron_enabled = $config->get('audit_export_enable_cron');

  // Check if cron processing is enabled.
  if (!$cron_enabled) {
    \Drupal::logger('audit_export')->debug('Audit export cron processing is disabled in settings.');
    return;
  }

  // Get additional settings.
  $run_every_cron = $config->get('audit_export_run_on_every_cron');

  // Default to daily if not set.
  $frequency = $config->get('audit_export_cron_frequency_default') ?: 1440;

  // Default to 2 minutes if not set.
  $queue_timeout = $config->get('audit_export_cron_queue_timeout') ?: 120;
  $last_run = \Drupal::state()->get('audit_export.last_cron_run', 0);

  // Determine whether we should run based on the settings.
  $time_elapsed = (time() - $last_run) >= ($frequency * 60);
  $should_run = $run_every_cron || ($time_elapsed && !$run_every_cron);

  if (!$should_run) {
    \Drupal::logger('audit_export')->debug('Skipping audit export cron - frequency condition not met.');
    return;
  }

  // Check if queue is already being processed - look for active lock.
  if (!\Drupal::lock()->acquire('audit_export_processing', 30)) {
    \Drupal::logger('audit_export')->info(
      'Skipping audit export cron - already in progress (locked).'
    );
    return;
  }

  try {
    // Check if the queue is empty before queueing new items.
    $queue = \Drupal::queue('audit_export_processor');
    $queue_is_empty = $queue->numberOfItems() == 0;

    // Only create new queue items if the queue is empty
    if ($queue_is_empty) {
      \Drupal::logger('audit_export')->notice('Starting audit export queue creation.');

      // Queue audit items for processing.
      /** @var \Drupal\audit_export_core\Cron\AuditExportCron $cron_service */
      $cron_service = \Drupal::service('audit_export_core.cron');

      // Pass configured timeout to the cron service.
      $items_queued = $cron_service->queueAudits($queue_timeout);

      if ($items_queued > 0) {
        \Drupal::logger('audit_export')->notice('Queued @count audit items for processing.', ['@count' => $items_queued]);
        // Update last run time when we queue new items.
        \Drupal::state()->set('audit_export.last_cron_run', time());
      }
      else {
        \Drupal::logger('audit_export')->notice('No audit items were queued for processing.');
      }
    }
    else {
      \Drupal::logger('audit_export')->info('Audit export queue already contains items, skipping new item creation.');
    }
  }
  catch (\Exception $e) {
    \Drupal::logger('audit_export')->error('Error during audit export cron: @message', ['@message' => $e->getMessage()]);
  }
  finally {
    // Always release the lock.
    \Drupal::lock()->release('audit_export_processing');
  }
}

/**
 * Implements hook_module_implements_alter().
 */
function audit_export_core_module_implements_alter(&$implementations, $hook) {
  // Move our cron implementation to the end to ensure all modules are loaded.
  if ($hook == 'cron' && isset($implementations['audit_export_core'])) {
    $group = $implementations['audit_export_core'];
    unset($implementations['audit_export_core']);
    $implementations['audit_export_core'] = $group;
  }
}

/**
 * Implements hook_schema_alter().
 */
function audit_export_core_schema_alter(&$schema) {
  if (isset($schema['key_value_expire']) && Database::getConnection()->schema()->tableExists('key_value_expire')) {
    // Increase expiration for queue items to allow for longer processing.
    try {
      $queue_records = Database::getConnection()->select('key_value_expire', 'kve')
        ->fields('kve', ['collection', 'name', 'value', 'expire'])
        ->condition('collection', 'queue')
        ->condition('name', 'audit_export_processor:%', 'LIKE')
        ->execute()
        ->fetchAll();

      foreach ($queue_records as $record) {
        Database::getConnection()->update('key_value_expire')
        // 24 hours
          ->fields(['expire' => time() + 86400])
          ->condition('collection', $record->collection)
          ->condition('name', $record->name)
          ->execute();
      }
    }
    catch (\Exception $e) {
      \Drupal::logger('audit_export')->error('Error updating queue expiration: @message', [
        '@message' => $e->getMessage(),
      ]);
    }
  }
}

/**
 * Implements hook_queue_info_alter().
 *
 * This allows us to set a custom time for our queue worker.
 */
function audit_export_core_queue_info_alter(&$queues) {
  if (isset($queues['audit_export_processor'])) {
    // Get the configured timeout from settings.
    $config = \Drupal::config('audit_export_core.settings');
    $timeout = $config->get('audit_export_cron_queue_timeout');

    // Only update if the setting exists and is valid.
    if ($timeout !== NULL && is_numeric($timeout) && $timeout > 0) {
      $queues['audit_export_processor']['cron']['time'] = (int) $timeout;

      \Drupal::logger('audit_export')->debug(
        'Set audit export queue timeout to @timeout seconds',
        ['@timeout' => $timeout]
      );
    }
  }
}
