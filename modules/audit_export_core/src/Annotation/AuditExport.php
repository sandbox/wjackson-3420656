<?php

namespace Drupal\audit_export_core\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an AuditExport annotation object.
 *
 * This annotation is used to define audit export plugins.
 *
 * @Annotation
 */
class AuditExport extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * A short description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $description;

  /**
   * The module to which this audit belongs.
   *
   * @var string
   */
  public $module;

  /**
   * The data type of the audit (e.g. process, flat, cross).
   *
   * @var string
   */
  public $data_type;

  /**
   * The unique identifier for this audit.
   *
   * @var string
   */
  public $identifier;

  /**
   * The group to which this audit belongs.
   *
   * @var string
   */
  public $group;

  /**
   * The dependencies for this audit.
   *
   * @var array
   */
  public $dependencies;

  /**
   * The class name implementing the audit logic.
   *
   * @var string
   */
  public $className;

}
