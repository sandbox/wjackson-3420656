<?php

namespace Drupal\audit_export_core\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\audit_export_core\Service\AuditExportAuditReport;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Process Audit Export items.
 *
 * @QueueWorker(
 *   id = "audit_export_processor",
 *   title = @Translation("Audit Export Processor"),
 *   cron = {"time" = 120}
 * )
 */
class AuditExportProcessor extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The audit export plugin manager.
   *
   * @var \Drupal\Core\Plugin\DefaultPluginManager
   */
  protected $pluginManager;

  /**
   * The audit export report service.
   *
   * @var \Drupal\audit_export_core\Service\AuditExportAuditReport
   */
  protected $auditReportService;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new AuditExportProcessor object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Plugin\DefaultPluginManager $plugin_manager
   *   The audit export plugin manager.
   * @param \Drupal\audit_export_core\Service\AuditExportAuditReport $audit_report_service
   *   The audit export report service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    DefaultPluginManager $plugin_manager,
    AuditExportAuditReport $audit_report_service,
    LoggerChannelFactoryInterface $logger_factory,
    ModuleHandlerInterface $module_handler,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->pluginManager = $plugin_manager;
    $this->auditReportService = $audit_report_service;
    $this->loggerFactory = $logger_factory;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.audit_export_audit'),
      $container->get('audit_export_core.audit_report'),
      $container->get('logger.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    try {
      // Verify plugin still exists.
      $definitions = $this->pluginManager->getDefinitions();
      if (!isset($definitions[$data['plugin_id']])) {
        throw new \Exception("Audit plugin {$data['plugin_id']} no longer exists");
      }

      // Create and verify plugin instance.
      $audit = $this->pluginManager->createInstance($data['plugin_id']);
      if (!$audit) {
        throw new \Exception("Could not create audit plugin instance: {$data['plugin_id']}");
      }

      // Only clear data if this is the first item for this plugin
      if (empty($data['continued_processing'])) {
        $this->auditReportService->clearReportData($data['plugin_id']);
      }

      // Check if we're processing a batch of data or a single item.
      if (!empty($data['batch_data'])) {
        // Process the batch of data.
        foreach ($data['batch_data'] as $row_data) {
          $processed_data = $audit->processData(['row_data' => $row_data]);
          $this->auditReportService->appendReportData($data['plugin_id'], $processed_data);
        }

        // Check if this is the last batch.
        if (!empty($data['batch_index']) && !empty($data['total_batches']) &&
          $data['batch_index'] == $data['total_batches'] - 1) {

          // Get latest data for the hook.
          $latest_data = $this->auditReportService->getReportData($data['plugin_id']);

          // Update timestamp after successful processing.
          $this->auditReportService->updateReportDate($data['plugin_id']);

          // Directly invoke the hook that audit_export_post implements.
          $this->moduleHandler->invokeAll('audit_export_process_complete', [
            $data['plugin_id'],
            $latest_data,
          ]);

          $this->loggerFactory->get('audit_export')->info(
            'Queue completed for @audit, hook_audit_export_process_complete invoked directly.',
            ['@audit' => $data['plugin_id']]
          );
        }
      }
      else {
        // Process a single row of data.
        $row_data = $data['row_data'] ?? NULL;
        if ($row_data) {
          $processed_data = $audit->processData(['row_data' => $row_data]);
          $this->auditReportService->appendReportData($data['plugin_id'], $processed_data);
        }
        else {
          // Process all data if no specific row data is provided.
          $audit->prepareData();

          // Get latest data for the hook.
          $latest_data = $this->auditReportService->getReportData($data['plugin_id']);

          // Update timestamp after successful processing.
          $this->auditReportService->updateReportDate($data['plugin_id']);

          // Directly invoke the hook that audit_export_post implements.
          $this->moduleHandler->invokeAll('audit_export_process_complete', [
            $data['plugin_id'],
            $latest_data,
          ]);

          $this->loggerFactory->get('audit_export')->info(
            'Queue completed for @audit, hook_audit_export_process_complete invoked directly.',
            ['@audit' => $data['plugin_id']]
          );
        }
      }
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('audit_export')->error(
        'Error processing audit @audit: @message', [
          '@audit' => $data['plugin_id'] ?? 'unknown',
          '@message' => $e->getMessage(),
        ]
      );
      // Re-throw to let the Queue API know this item failed.
      throw $e;
    }
  }

}
