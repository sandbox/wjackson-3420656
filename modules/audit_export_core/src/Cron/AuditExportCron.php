<?php

namespace Drupal\audit_export_core\Cron;

use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Lock\LockBackendInterface;

/**
 * Provides cron functionality for Audit Export.
 */
class AuditExportCron {

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The lock backend.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * The audit queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * Constructs a new AuditExportCron object.
   */
  public function __construct(
    QueueFactory $queue_factory,
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_factory,
    StateInterface $state,
    ModuleHandlerInterface $module_handler,
    LockBackendInterface $lock,
  ) {
    $this->queueFactory = $queue_factory;
    $this->configFactory = $config_factory;
    $this->loggerFactory = $logger_factory;
    $this->state = $state;
    $this->moduleHandler = $module_handler;
    $this->lock = $lock;
    $this->queue = $queue_factory->get('audit_export_processor');
  }

  /**
   * Queues all audits for processing.
   *
   * This adds items to the queue but doesn't process them.
   * Processing will be handled by the QueueWorker plugin.
   *
   * @param int $queue_timeout
   *   The timeout value for queue processing in seconds.
   *
   * @return int
   *   The number of items queued.
   */
  public function queueAudits($queue_timeout = 120) {
    // Get all audit plugins from the plugin manager.
    $plugin_manager = \Drupal::service('plugin.manager.audit_export_audit');

    try {
      $definitions = $plugin_manager->getDefinitions();
      $count = 0;

      // Start fresh - clear any existing items in the queue.
      $this->queue->deleteQueue();

      // Clear any tracking of previously queued plugins.
      $this->state->delete('audit_export.queued_plugins');

      // Set queue worker time in state for the queue worker to access.
      $this->state->set('audit_export.queue_timeout', $queue_timeout);

      foreach ($definitions as $plugin_id => $definition) {
        // For each plugin, get all the data that needs to be processed.
        try {
          $plugin_instance = $plugin_manager->createInstance($plugin_id);
          if (!$plugin_instance) {
            $this->loggerFactory->get('audit_export')->error(
              'Could not create audit plugin instance: @plugin_id',
              ['@plugin_id' => $plugin_id]
            );
            continue;
          }

          $data_to_process = $plugin_instance->prepareData();

          if (empty($data_to_process)) {
            // If no data to process, queue placeholder to clear existing data.
            $this->queue->createItem([
              'plugin_id' => $plugin_id,
              'group' => $definition['group'],
              'empty_data' => TRUE,
            ]);
            $count++;

            $this->loggerFactory->get('audit_export')->debug(
              'Queued empty item for @plugin_id (no data available)',
              ['@plugin_id' => $plugin_id]
            );
          }
          else {
            // Calculate optimal batch size based on data size and timeout
            // Aim for batches that can be processed within the timeout.
            $total_items = count($data_to_process);
            // Adjust based on actual performance.
            $estimated_items_per_minute = 300;
            $items_per_second = $estimated_items_per_minute / 60;
            // At least 10, at most 200 per batch.
            $items_per_timeout = max(10, min(200, ceil($items_per_second * $queue_timeout)));

            // Split data into batches for processing.
            $batches = array_chunk($data_to_process, $items_per_timeout);

            foreach ($batches as $index => $batch) {
              $this->queue->createItem([
                'plugin_id' => $plugin_id,
                'group' => $definition['group'],
                'batch_data' => $batch,
                'batch_index' => $index,
                'total_batches' => count($batches),
                'continued_processing' => $index > 0,
              ]);
              $count++;
            }

            $this->loggerFactory->get('audit_export')->debug(
              'Queued @plugin_id audit with @items items in @batches batches',
              [
                '@plugin_id' => $plugin_id,
                '@items' => $total_items,
                '@batches' => count($batches),
              ]
            );
          }
        }
        catch (\Exception $e) {
          $this->loggerFactory->get('audit_export')->error(
            'Error queuing plugin @plugin_id: @message',
            ['@plugin_id' => $plugin_id, '@message' => $e->getMessage()]
          );
        }
      }

      return $count;
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('audit_export')->error(
        'Error queuing audits: @message',
        ['@message' => $e->getMessage()]
      );
      return 0;
    }
  }

  /**
   * Checks if a plugin is already queued for processing.
   *
   * @param string $plugin_id
   *   The plugin ID to check.
   *
   * @return bool
   *   TRUE if the plugin is already queued, FALSE otherwise.
   */
  protected function isAlreadyQueued($plugin_id) {
    // Use the state system to check if a plugin is already queued.
    $queued_plugins = $this->state->get('audit_export.queued_plugins', []);

    if (isset($queued_plugins[$plugin_id])) {
      // Check if the queue entry has expired (more than 1 hour old)
      if ((time() - $queued_plugins[$plugin_id]) > 3600) {
        // Entry has expired, remove it.
        unset($queued_plugins[$plugin_id]);
        $this->state->set('audit_export.queued_plugins', $queued_plugins);
        return FALSE;
      }
      return TRUE;
    }

    // Add this plugin to the queued plugins list.
    $queued_plugins[$plugin_id] = time();
    $this->state->set('audit_export.queued_plugins', $queued_plugins);
    return FALSE;
  }

}
