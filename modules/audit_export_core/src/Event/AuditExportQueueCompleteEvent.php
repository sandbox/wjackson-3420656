<?php

namespace Drupal\audit_export_core\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that is fired when a queue job completes processing an audit.
 */
class AuditExportQueueCompleteEvent extends Event {

  /**
   * The event name.
   */
  const EVENT_NAME = 'audit_export_core.queue_complete';

  /**
   * The audit name.
   *
   * @var string
   */
  protected $auditName;

  /**
   * Constructs a new AuditExportQueueCompleteEvent.
   *
   * @param string $audit_name
   *   The audit name.
   */
  public function __construct(string $audit_name) {
    $this->auditName = $audit_name;
  }

  /**
   * Gets the audit name.
   *
   * @return string
   *   The audit name.
   */
  public function getAuditName() {
    return $this->auditName;
  }

}
