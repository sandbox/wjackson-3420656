<?php

namespace Drupal\audit_export_core\Service;

use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Class used for building the AuditExportAuditGroup object.
 */
class AuditExportAuditGroup {

  /**
   * Machine name for the audit group.
   *
   * @var string
   */
  public string $name;

  /**
   * Human-readable name for the audit group.
   *
   * @var string
   */
  public string $label;

  /**
   * Optional description for the audit group.
   *
   * @var string
   */
  public string $description;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Constructs an AuditExportAuditGroup object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   */
  public function __construct(ModuleHandlerInterface $moduleHandler) {
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Retrieves all audit groups.
   *
   * @return array
   *   An array of audit groups.
   */
  public function getAuditGroups(): array {
    return $this->moduleHandler->invokeAll('aex_group');
  }

}
