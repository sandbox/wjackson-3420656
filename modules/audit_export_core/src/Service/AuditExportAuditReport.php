<?php

namespace Drupal\audit_export_core\Service;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Session\AccountInterface;

/**
 * Service class for handling audit export reports.
 */
class AuditExportAuditReport {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The audit export plugin manager.
   *
   * @var \Drupal\Core\Plugin\DefaultPluginManager
   */
  protected $pluginManager;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a new AuditExportAuditReport object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Plugin\DefaultPluginManager $plugin_manager
   *   The plugin manager for audit export plugins.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel for audit export.
   */
  public function __construct(
    Connection $database,
    AccountInterface $current_user,
    DateFormatterInterface $date_formatter,
    EntityTypeManagerInterface $entity_type_manager,
    DefaultPluginManager $plugin_manager,
    CacheBackendInterface $cache_backend,
    LoggerChannelInterface $logger,
  ) {
    $this->database = $database;
    $this->currentUser = $current_user;
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->pluginManager = $plugin_manager;
    $this->cacheBackend = $cache_backend;
    $this->logger = $logger;
  }

  /**
   * Retrieves the report data from the database.
   *
   * @param string $audit_name
   *   The name of the audit.
   *
   * @return array
   *   The unserialized report data, or an empty array if not found.
   */
  public function getReportData($audit_name) {
    try {
      // Fetch the data from the database.
      $data = $this->database->select('audit_export_report', 'aer')
        ->fields('aer', ['data'])
        ->condition('audit', $audit_name)
        ->execute()
        ->fetchField();

      // Process the data, ensuring we handle it safely.
      $report_data = [];
      if (!empty($data)) {
        // First try to decode it as JSON, which is the safest approach.
        // If the data isn't JSON, this will return NULL.
        $decoded = @json_decode($data, TRUE);

        if ($decoded !== NULL) {
          // If it was valid JSON, use that data.
          $report_data = $decoded;
        }
        else {
          // If it wasn't JSON, use a custom function to extract just the data
          // without trying to instantiate any objects.
          $report_data = $this->extractSerializedArrayData($data);
        }
      }

      return $report_data;
    }
    catch (\Exception $e) {
      $this->logger->error(
        'Error retrieving the report data: @message',
        ['@message' => $e->getMessage()]
      );
      return [];
    }
  }

  /**
   * Extracts array data from serialized string without instantiating objects.
   *
   * This is a fallback method to handle serialized data that might contain
   * objects we can't properly unserialize due to class loading issues.
   *
   * @param string $serialized
   *   The serialized data string.
   *
   * @return array
   *   The extracted array data, with objects converted to strings.
   */
  private function extractSerializedArrayData($serialized) {
    // Try to safely unserialize first.
    try {
      // Try with no classes allowed.
      return unserialize($serialized, ['allowed_classes' => FALSE]) ?: [];
    }
    catch (\Exception $e) {
      $this->logger->warning(
        'Failed to unserialize data: @message. Using emergency extraction.',
        ['@message' => $e->getMessage()]
      );

      // Fall back to a simple array extraction.
      // This is a crude approach but should work for simple data.
      $result = [];

      // Check if this looks like serialized data.
      if (preg_match('/^a:\d+:/', $serialized)) {
        // Extract key-value pairs from the serialized array.
        preg_match_all('/(s:\d+:"[^"]+";(?:s|i|d|b|a):[^;]+;)/', $serialized, $matches);

        if (!empty($matches[0])) {
          foreach ($matches[0] as $pair) {
            // Extract key.
            preg_match('/s:\d+:"([^"]+)";/', $pair, $key_match);
            // Extract value, but only handle strings, integers, and booleans.
            preg_match('/";\s*(s:\d+:"[^"]+"|i:\d+|b:[01])/', $pair, $value_match);

            if (!empty($key_match[1]) && !empty($value_match[1])) {
              $key = $key_match[1];
              $value = $value_match[1];

              // Convert the value to appropriate type.
              if (strpos($value, 's:') === 0) {
                preg_match('/s:\d+:"([^"]+)"/', $value, $str_match);
                $value = !empty($str_match[1]) ? $str_match[1] : '';
              }
              elseif (strpos($value, 'i:') === 0) {
                preg_match('/i:(\d+)/', $value, $int_match);
                $value = !empty($int_match[1]) ? (int) $int_match[1] : 0;
              }
              elseif (strpos($value, 'b:') === 0) {
                $value = (substr($value, -1) === '1');
              }

              $result[$key] = $value;
            }
          }
        }
      }

      return $result;
    }
  }

  /**
   * Saves the report to the database, replacing any existing record.
   *
   * @param string $audit_name
   *   The audit name.
   * @param array $newRecord
   *   The new report data.
   *
   * @throws \Exception
   */
  public function saveReport($audit_name, array $newRecord) {
    // Current timestamp.
    $date = time();
    // Current user ID.
    $author = $this->currentUser->id();

    try {
      // Process the data to convert objects to simple types.
      $sanitizedRecord = $this->flattenDataForStorage($newRecord);

      // Check if a record for the given audit already exists.
      $existing_record = $this->database->select('audit_export_report', 'aer')
        ->fields('aer', ['data'])
        ->condition('audit', $audit_name)
        ->execute()
        ->fetchField();

      // Store data as JSON instead of serialized PHP to avoid object issues.
      $dataToStore = json_encode([$sanitizedRecord], JSON_PRETTY_PRINT);

      if ($existing_record) {
        // Update the existing record, replacing the data with the new record.
        $this->database->update('audit_export_report')
          ->fields([
            'date' => $date,
            // Store as JSON instead of serialized PHP.
            'data' => $dataToStore,
          ])
          ->condition('audit', $audit_name)
          ->execute();
      }
      else {
        // Insert a new record if no existing record is found.
        $this->database->insert('audit_export_report')
          ->fields([
            'audit' => $audit_name,
            'author' => $author,
            'date' => $date,
            'data' => $dataToStore,
          ])
          ->execute();
      }

      // Invoke hook_audit_export_process_complete with the sanitized data.
      \Drupal::moduleHandler()->invokeAll(
        'audit_export_process_complete',
        [$audit_name, [$sanitizedRecord]]
      );
    }
    catch (\Exception $e) {
      // Log the error and rethrow the exception for debugging.
      $this->logger->error(
        'Error saving the report: @message',
        ['@message' => $e->getMessage()]
      );
      throw new \Exception('Error saving the report: ' . $e->getMessage());
    }
  }

  /**
   * Flattens data for storage by converting objects to simple types.
   *
   * @param mixed $data
   *   The data to flatten.
   *
   * @return mixed
   *   The flattened data.
   */
  private function flattenDataForStorage($data) {
    if (is_array($data)) {
      $result = [];
      foreach ($data as $key => $value) {
        $result[$key] = $this->flattenDataForStorage($value);
      }
      return $result;
    }
    elseif (is_object($data)) {
      // Convert TranslatableMarkup directly to string when possible.
      if (method_exists($data, '__toString')) {
        try {
          return (string) $data;
        }
        catch (\Exception $e) {
          // If casting to string fails, return a placeholder.
          $this->logger->warning(
            'Failed to convert object to string: @message',
            ['@message' => $e->getMessage()]
          );
          return '[Object]';
        }
      }

      // For standard objects, convert to array.
      if ($data instanceof \stdClass) {
        return (array) $data;
      }

      // For other objects, return a simple indicator.
      $className = get_class($data);
      return "[Object: $className]";
    }
    else {
      return $data;
    }
  }

  /**
   * Retrieves all audit reports with their last processed dates.
   *
   * @return array
   *   An array of reports with audit names and formatted last processed dates.
   */
  public function getAllReports() {
    $reports = [];

    try {
      // Fetch all audit reports from the database.
      $result = $this->database->select('audit_export_report', 'aer')
        ->fields('aer', ['audit', 'date'])
        ->execute();

      foreach ($result as $record) {
        // Format the date using the date formatter service.
        $formatted_date = $this->dateFormatter->format(
          $record->date,
          'custom',
          'd/M/Y g:i A'
        );

        $reports[] = [
          'audit' => $record->audit,
          'last_processed' => $formatted_date,
        ];
      }
    }
    catch (\Exception $e) {
      $this->logger->error(
        'Error retrieving reports: @message',
        ['@message' => $e->getMessage()]
      );
    }

    return $reports;
  }

  /**
   * Retrieves the last processed date for a specific audit report.
   *
   * @param string $audit_name
   *   The name of the audit.
   *
   * @return string|null
   *   The formatted last processed date or null if the report hasn't been run.
   */
  public function getLastProcessedDate($audit_name) {
    try {
      // Fetch the last processed date from the database.
      $date = $this->database->select('audit_export_report', 'aer')
        ->fields('aer', ['date'])
        ->condition('audit', $audit_name)
        ->execute()
        ->fetchField();

      // Format the date if it exists, otherwise return null.
      return $date
        ? $this->dateFormatter->format($date, 'custom', 'm/d/Y g:i A')
        : NULL;
    }
    catch (\Exception $e) {
      $this->logger->error(
        'Error retrieving last processed date for @audit: @message',
        ['@audit' => $audit_name, '@message' => $e->getMessage()]
      );
      return NULL;
    }
  }

  /**
   * Clears the data for a specific audit report.
   *
   * Sets the date to the current time.
   *
   * @param string $audit_name
   *   The name of the audit.
   */
  public function clearReportData($audit_name) {
    try {
      // Get the current timestamp.
      $current_time = time();

      // Check if the record exists first.
      $existing_record = $this->database->select('audit_export_report', 'aer')
        ->fields('aer', ['audit'])
        ->condition('audit', $audit_name)
        ->execute()
        ->fetchField();

      // Use JSON encoding for empty array - safer than serialize.
      $empty_data = json_encode([]);

      if ($existing_record) {
        // Update the existing record to clear the data and set the date.
        $this->database->update('audit_export_report')
          ->fields([
            // Clear the data.
            'data' => $empty_data,
            // Set the date to the current time.
            'date' => $current_time,
          ])
          ->condition('audit', $audit_name)
          ->execute();
      }
      else {
        // Insert a new record with empty data if it doesn't exist.
        $this->database->insert('audit_export_report')
          ->fields([
            'audit' => $audit_name,
            'author' => $this->currentUser->id(),
            'date' => $current_time,
            'data' => $empty_data,
          ])
          ->execute();
      }
    }
    catch (\Exception $e) {
      $this->logger->error(
        'Error clearing report data for @audit: @message',
        ['@audit' => $audit_name, '@message' => $e->getMessage()]
      );
    }
  }

  /**
   * Appends processed data to the existing report.
   *
   * @param string $audit_name
   *   The name of the audit.
   * @param array $newRecord
   *   The new record to append.
   */
  public function appendReportData($audit_name, array $newRecord) {
    try {
      // Flatten the new record to avoid object serialization issues.
      $flattenedRecord = $this->flattenDataForStorage($newRecord);

      // Fetch existing data.
      $existing_data = $this->database->select('audit_export_report', 'aer')
        ->fields('aer', ['data'])
        ->condition('audit', $audit_name)
        ->execute()
        ->fetchField();

      // First try to decode as JSON.
      $decoded = @json_decode($existing_data, TRUE);

      if ($decoded !== NULL) {
        // It was JSON, add the new record.
        $existing_array = $decoded;
      }
      else {
        // Try to get data from serialized data if not JSON.
        $existing_array = $this->extractSerializedArrayData($existing_data);
      }

      if (!is_array($existing_array)) {
        $existing_array = [];
      }

      // Append the new record.
      $existing_array[] = $flattenedRecord;

      // Store as JSON.
      $new_data = json_encode($existing_array, JSON_PRETTY_PRINT);

      // Update the report with the appended data.
      $this->database->update('audit_export_report')
        ->fields(['data' => $new_data])
        ->condition('audit', $audit_name)
        ->execute();
    }
    catch (\Exception $e) {
      $this->logger->error(
        'Error appending report data for @audit: @message',
        ['@audit' => $audit_name, '@message' => $e->getMessage()]
      );
    }
  }

  /**
   * Updates the date field of the report to the current time.
   *
   * @param string $audit_name
   *   The name of the audit.
   */
  public function updateReportDate($audit_name) {
    try {
      // Get the current timestamp.
      $current_time = time();

      // Update the date to the current time.
      $this->database->update('audit_export_report')
        ->fields(['date' => $current_time])
        ->condition('audit', $audit_name)
        ->execute();

      // Get the latest data for the hook.
      $data = $this->getReportData($audit_name);

      // Invoke the hook_audit_export_process_complete hook.
      \Drupal::moduleHandler()->invokeAll(
        'audit_export_process_complete',
        [$audit_name, $data]
      );
    }
    catch (\Exception $e) {
      $this->logger->error(
        'Error updating report date for @audit: @message',
        ['@audit' => $audit_name, '@message' => $e->getMessage()]
      );
    }
  }

  /**
   * Updates report dates for multiple audits in a batch.
   *
   * @param array $audit_results
   *   An array of audit results from batch processing.
   */
  public function updateBatchReportDates(array $audit_results) {
    foreach ($audit_results as $audit_name => $audit_data) {
      if (!empty($audit_data['success'])) {
        $this->updateReportDate($audit_name);
      }
    }
  }

  /**
   * Gets the report metadata including creation timestamp and author.
   *
   * @param string $audit_name
   *   The name of the audit.
   *
   * @return array
   *   An array containing report metadata.
   */
  public function getReportMetadata($audit_name) {
    try {
      $result = $this->database->select('audit_export_report', 'aer')
        ->fields('aer', ['date', 'author'])
        ->condition('audit', $audit_name)
        ->execute()
        ->fetchAssoc();

      if ($result) {
        return [
          'date' => $result['date'],
          'author' => $result['author'],
        ];
      }
    }
    catch (\Exception $e) {
      $this->logger->error(
        'Error getting report metadata for @audit: @message',
        ['@audit' => $audit_name, '@message' => $e->getMessage()]
      );
    }

    return [
      'date' => time(),
      'author' => $this->currentUser->id(),
    ];
  }

}
