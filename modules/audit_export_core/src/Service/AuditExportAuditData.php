<?php

namespace Drupal\audit_export_core\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class for building the AuditExportAuditData object.
 */
class AuditExportAuditData {

  use StringTranslationTrait;

  /**
   * Constant for flat data type.
   */
  const DATA_TYPE_FLAT = 'flat';

  /**
   * Constant for cross data type.
   */
  const DATA_TYPE_CROSS = 'cross';

  /**
   * Constant for process data type.
   */
  const DATA_TYPE_PROCESS = 'process';

  /**
   * The data headers array.
   *
   * @var array
   */
  protected $dataHeaders = [];

  /**
   * The cross-tab header label.
   *
   * @var string
   */
  protected $crossTabHeaderLabel = '';

  /**
   * The data type.
   *
   * @var string
   */
  protected $dataType;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param string|null $type
   *   The data type to use, defaults to flat.
   */
  public function __construct(ConfigFactoryInterface $configFactory, ModuleHandlerInterface $moduleHandler, ?string $type = NULL) {
    $this->configFactory = $configFactory;
    $this->moduleHandler = $moduleHandler;
    $this->setDataType($type ?? self::DATA_TYPE_FLAT);
  }

  /**
   * Sets the headers for the data.
   *
   * @param array $headers
   *   An array of header values.
   */
  public function setHeaders(array $headers): void {
    $this->dataHeaders = $headers;
  }

  /**
   * Sets the CrossTab header label.
   *
   * @param string $crossTabHeaderLabel
   *   The cross-tab header label to set.
   */
  public function setCrossTabHeaderLabel(string $crossTabHeaderLabel): void {
    $this->crossTabHeaderLabel = $crossTabHeaderLabel;
  }

  /**
   * Gets the CrossTab header label.
   *
   * @return string
   *   The cross-tab header label.
   */
  public function getCrossTabHeaderLabel(): string {
    return $this->crossTabHeaderLabel;
  }

  /**
   * Gets the configuration for the given audit.
   *
   * @param object $audit
   *   The audit object.
   *
   * @return array
   *   An array of configuration values.
   */
  public function getConfig($audit): array {
    $config = [];

    $option_overrides = [
      'audit_export_override',
      'audit_export_enable_cron',
      'audit_export_cron_queue_timeout',
      'audit_export_cron_frequency_default',
    ];

    foreach ($option_overrides as $override_option) {
      $config[$override_option . '_' . $audit->name] = $this->configFactory->get('audit_export.settings')->get($override_option . '_' . $audit->name);
    }

    return $config;
  }

  /**
   * Gets the headers.
   *
   * @return array
   *   An array of headers including the cross-tab header if present.
   */
  public function getHeaders(): array {
    // Add CrossTabHeaderLabel, if available.
    $headers = [];
    if (!empty($this->getCrossTabHeaderLabel())) {
      $headers[] = $this->getCrossTabHeaderLabel();
    }
    return array_merge($headers, $this->dataHeaders);
  }

  /**
   * Sets the data type.
   *
   * @param string $type
   *   The data type to set.
   *
   * @throws \InvalidArgumentException
   *   If an invalid data type is provided.
   */
  public function setDataType(string $type): void {
    $allowedTypes = [self::DATA_TYPE_CROSS, self::DATA_TYPE_FLAT, self::DATA_TYPE_PROCESS];
    if (!in_array($type, $allowedTypes)) {
      throw new \InvalidArgumentException(sprintf("Invalid data type. Allowed types are: %s", implode(", ", $allowedTypes)));
    }
    $this->dataType = $type;
  }

  /**
   * Gets the data type.
   *
   * @return string
   *   The data type.
   */
  public function getDataType(): string {
    return $this->dataType;
  }

  /**
   * Gets the data for the audit.
   *
   * @param object $audit
   *   The audit object.
   *
   * @return array
   *   An array of data from the last report.
   */
  public function getData($audit): array {
    $report_service = \Drupal::service('audit_export_core.audit_export_audit_report');
    return $report_service->getLastReport($audit);
  }

  /**
   * Prepares CrossTab scripts.
   *
   * @return array
   *   An array of CrossTab scripts.
   */
  public function prepareCrossTabScripts(): array {
    return [];
  }

  /**
   * Prepares data.
   *
   * @return array
   *   An array of prepared data.
   */
  public function prepareData(): array {
    return [];
  }

  /**
   * Processes data.
   *
   * @param array $params
   *   Parameters for data processing.
   *
   * @return array
   *   An array of processed data.
   */
  public function processData(array $params = []): array {
    return [];
  }

  /**
   * Processes data pre-processing.
   *
   * @return array
   *   An array of pre-processing operations.
   */
  public function processDataPreProcess(): array {
    return [];
  }

}
