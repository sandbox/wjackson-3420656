<?php

namespace Drupal\audit_export_core\Service;

use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Asset\AssetResolverInterface;

/**
 * Service for displaying audit export data.
 *
 * Handles rendering display tables and processing special characters.
 */
class AuditExportDisplay {

  /**
   * The display options.
   *
   * @var array
   */
  protected $options;

  /**
   * The table header rows.
   *
   * @var array
   */
  protected $headerRows;

  /**
   * The table data rows.
   *
   * @var array
   */
  protected $dataRows;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The asset resolver service.
   *
   * @var \Drupal\Core\Asset\AssetResolverInterface
   */
  protected $assetResolver;

  /**
   * Constructs a new AuditExportDisplay object.
   *
   * @param array $options
   *   The display options.
   * @param array $header_rows
   *   The table header rows.
   * @param array $data_rows
   *   The table data rows.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Asset\AssetResolverInterface $assetResolver
   *   The asset resolver service.
   */
  public function __construct(array $options, array $header_rows, array $data_rows, RendererInterface $renderer, AssetResolverInterface $assetResolver) {
    $this->setHeaderRows($header_rows);
    $this->setDataRows($data_rows);
    $this->setOptions($options);
    $this->renderer = $renderer;
    $this->assetResolver = $assetResolver;
  }

  /**
   * Sets the header rows for the display.
   *
   * @param array $header_rows
   *   The header rows to set.
   */
  private function setHeaderRows($header_rows): void {
    $this->headerRows = $header_rows;
  }

  /**
   * Sets the data rows for the display.
   *
   * @param array $data_rows
   *   The data rows to set.
   */
  private function setDataRows($data_rows): void {
    $this->dataRows = $data_rows;
  }

  /**
   * Sets the options for the display.
   *
   * @param array $options
   *   The options to set.
   */
  private function setOptions($options): void {
    $this->options = $options;
  }

  /**
   * Renders the display with the provided data.
   *
   * @param array $header_rows
   *   The header rows for the table.
   * @param array $data_rows
   *   The data rows for the table.
   * @param array $options
   *   Optional rendering options.
   *
   * @return array
   *   A render array for the display.
   */
  public function renderDisplay(array $header_rows, array $data_rows, array $options = []): array {
    // Add CSS using the asset library system.
    $build = [
      '#attached' => [
        'library' => ['audit_export/audit_export.styles'],
      ],
    ];

    // Prepare the render array for the table.
    $build['table'] = [
      '#theme' => 'table',
      '#header' => $header_rows,
      '#rows' => $data_rows,
      '#attributes' => ['class' => ['audit-export']],
      '#empty' => $options['empty'] ?? NULL,
    ];

    return $build;
  }

  /**
   * Cleans up display tokens in the provided data.
   *
   * @param array $data
   *   The data array to process.
   * @param array $replacementMap
   *   Optional mapping of tokens to their replacements.
   *
   * @return array
   *   The processed data with tokens cleaned up.
   */
  public static function tokenDisplayCleanup(array $data, array $replacementMap = []): array {
    if (empty($replacementMap)) {
      $replacementMap = self::getReplacementMap();
    }

    foreach ($data as &$subArray) {
      if (is_array($subArray)) {
        foreach ($subArray as &$value) {
          if (is_string($value)) {
            $value = str_replace(array_keys($replacementMap), array_values($replacementMap), $value);
          }
        }
      }
    }

    return $data;
  }

  /**
   * Gets the default replacement map for token cleanup.
   *
   * @return array
   *   The default replacement mapping.
   */
  public static function getReplacementMap(): array {
    return [
      '<front>' => '&lt;front&gt;',
    ];
  }

}
