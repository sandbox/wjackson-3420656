<?php

namespace Drupal\audit_export_core\Drush\Commands;

use Drupal\audit_export_core\Cron\AuditExportCron;
use Drupal\audit_export_core\Service\AuditExportAuditReport;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\State\StateInterface;
use Drush\Commands\DrushCommands;

/**
 * Drush commands for Audit Export module.
 */
class AuditExportCoreCommands extends DrushCommands {

  /**
   * The audit export plugin manager.
   *
   * @var \Drupal\Core\Plugin\DefaultPluginManager
   */
  protected $pluginManager;

  /**
   * The audit export report service.
   *
   * @var \Drupal\audit_export_core\Service\AuditExportAuditReport
   */
  protected $auditReportService;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module extension list service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * The queue factory service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The cron service.
   *
   * @var \Drupal\audit_export_core\Cron\AuditExportCron
   */
  protected $cronService;

  /**
   * Constructs a new AuditExportCoreCommands object.
   *
   * @param \Drupal\Core\Plugin\DefaultPluginManager $plugin_manager
   *   The plugin manager for audit export plugins.
   * @param \Drupal\audit_export_core\Service\AuditExportAuditReport $audit_report_service
   *   The audit export report service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   *   The module extension list service.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\audit_export_core\Cron\AuditExportCron $cron_service
   *   The audit export cron service.
   */
  public function __construct(
    DefaultPluginManager $plugin_manager,
    AuditExportAuditReport $audit_report_service,
    AccountProxyInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    ModuleExtensionList $module_extension_list,
    QueueFactory $queue_factory,
    StateInterface $state,
    AuditExportCron $cron_service,
  ) {
    parent::__construct();
    $this->pluginManager = $plugin_manager;
    $this->auditReportService = $audit_report_service;
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->moduleExtensionList = $module_extension_list;
    $this->queueFactory = $queue_factory;
    $this->state = $state;
    $this->cronService = $cron_service;
  }

  /**
   * Lists all available audit reports.
   *
   * @command audit-export:list
   * @aliases aexl
   * @usage audit-export:list
   *   Lists all available audit reports.
   *
   * @group Audit Export
   */
  public function listAudits() {
    $plugin_definitions = $this->pluginManager->getDefinitions();

    if (empty($plugin_definitions)) {
      $this->logger()->notice('No audit reports are available.');
      return;
    }

    // Set up the table header.
    $rows = [];
    $rows[] = [
      'Audit',
      'ID',
      'Group',
      'Description',
      'Module',
      'Dependencies',
      'Last Processed',
    ];

    // Add a row for each audit plugin.
    foreach ($plugin_definitions as $id => $definition) {
      $last_processed = $this->auditReportService->getLastProcessedDate($id) ?? 'Never';

      $rows[] = [
        $definition['label'],
        $id,
        $definition['group'] ?? 'general',
        $definition['description'] ?? '',
        $definition['provider'] ?? 'audit_export_core',
        !empty($definition['dependencies']) ? implode(', ', $definition['dependencies']) : '',
        $last_processed,
      ];
    }

    // Display the table.
    $this->io()->table([], $rows);
  }

  /**
   * Processes an audit report or all reports.
   *
   * @param string $audit_id
   *   The ID of the audit to process. If not provided, an interactive choice
   *   will be displayed.
   * @param array $options
   *   An array of options from the command line.
   *
   * @option all Process all audit reports.
   *
   * @command audit-export:run
   * @aliases aexp,audit-export:process
   * @usage audit-export:run
   *   Interactively choose which audit report to process.
   * @usage audit-export:run --all
   *   Process all audit reports.
   * @usage audit-export:run content_type_audit
   *   Process the Content Type Audit report.
   *
   * @group Audit Export
   */
  public function processAudit($audit_id = NULL, array $options = ['all' => FALSE]) {
    $plugin_definitions = $this->pluginManager->getDefinitions();

    if (empty($plugin_definitions)) {
      $this->logger()->error('No audit reports are available.');
      return;
    }

    // Process all audits if the --all option is specified.
    if ($options['all']) {
      $this->logger()->notice('Processing all audit reports...');

      foreach ($plugin_definitions as $id => $definition) {
        $this->processSingleAudit($id);
      }

      $this->logger()->success('All audit reports have been processed.');
      return;
    }

    // If no audit ID is provided, offer an interactive choice.
    if (empty($audit_id)) {
      $choice_options = [];

      foreach ($plugin_definitions as $id => $definition) {
        $choice_options[$id] = $definition['label'] . ' (' . $id . ')';
      }

      $audit_id = $this->io()->choice(
        'Which report should be processed?',
        $choice_options
      );
    }

    // Validate the audit ID.
    if (!isset($plugin_definitions[$audit_id])) {
      $this->logger()->error(sprintf('Audit report "%s" does not exist.', $audit_id));
      return;
    }

    // Process the selected audit.
    $this->processSingleAudit($audit_id);
    $this->logger()->success(
      sprintf('Audit report "%s" has been processed.', $audit_id)
    );
  }

  /**
   * Processes a single audit report.
   *
   * @param string $audit_id
   *   The ID of the audit to process.
   */
  protected function processSingleAudit($audit_id) {
    // Get the audit plugin instance.
    $audit_plugin = $this->pluginManager->createInstance($audit_id);

    if (!$audit_plugin) {
      $this->logger()->error(sprintf('Failed to load audit plugin "%s".', $audit_id));
      return;
    }

    // Clear existing data for the audit.
    $this->auditReportService->clearReportData($audit_id);

    // Prepare and process data.
    $report_data = $audit_plugin->prepareData();
    $this->logger()->notice(
      sprintf('Processing audit "%s" with %d items...', $audit_id, count($report_data))
    );

    $progress = $this->io()->createProgressBar(count($report_data));
    $progress->start();

    foreach ($report_data as $data) {
      try {
        // Process data using the audit plugin's processData method.
        $processed_data = $audit_plugin->processData(['row_data' => $data]);

        // Append processed data to the report.
        $this->auditReportService->appendReportData($audit_id, $processed_data);

      }
      catch (\Exception $e) {
        $this->logger()->error(
          sprintf('Error processing audit "%s": %s', $audit_id, $e->getMessage())
        );
      }

      $progress->advance();
    }

    $progress->finish();
    $this->io()->newLine(2);

    // Update the report date after successful processing.
    $this->auditReportService->updateReportDate($audit_id);
  }

  /**
   * Exports an audit report to CSV.
   *
   * @param string $audit_id
   *   The ID of the audit to export.
   * @param array $options
   *   An array of options from the command line.
   *
   * @option destination The file path where the CSV will be saved.
   *
   * @command audit-export:export
   * @aliases aexe
   * @usage audit-export:export content_type_audit
   *   Export the Content Type Audit report to CSV.
   * @usage audit-export:export content_type_audit --destination=/path/to/file.csv
   *   Export the Content Type Audit report to the specified file.
   *
   * @group Audit Export
   */
  public function exportAudit($audit_id, array $options = ['destination' => NULL]) {
    $plugin_definitions = $this->pluginManager->getDefinitions();

    // Validate the audit ID.
    if (!isset($plugin_definitions[$audit_id])) {
      $this->logger()->error(sprintf('Audit report "%s" does not exist.', $audit_id));
      return;
    }

    // Get the audit plugin instance.
    $audit_plugin = $this->pluginManager->createInstance($audit_id);

    // Get the report data.
    $report_data = $this->auditReportService->getReportData($audit_id);

    if (empty($report_data)) {
      $this->logger()->warning(
        sprintf('No data available for audit report "%s". Process the report first.', $audit_id)
      );
      return;
    }

    // Determine the destination file.
    $destination = $options['destination'];
    if (!$destination) {
      $timestamp = date('Y-m-d_H-i-s');
      $destination = sprintf(
        '%s/audit_export_%s_%s.csv',
        sys_get_temp_dir(),
        $audit_id,
        $timestamp
      );
    }

    // Create the CSV file.
    $headers = $audit_plugin->getHeaders();
    $fp = fopen($destination, 'w');

    // Write headers.
    fputcsv($fp, $headers);

    // Write data rows.
    foreach ($report_data as $row) {
      fputcsv($fp, $row);
    }

    fclose($fp);

    $this->logger()->success(
      sprintf('Audit report "%s" has been exported to %s', $audit_id, $destination)
    );
  }

  /**
   * Runs an audit report via queue processing.
   *
   * @param string $audit_id
   *   The ID of the audit to queue for processing.
   * @param array $options
   *   An array of options from the command line.
   *
   * @option all Queue all audit reports for processing.
   *
   * @command audit-export:queue
   * @aliases aexq
   * @usage audit-export:queue content_type_audit
   *   Queue the Content Type Audit report for processing.
   * @usage audit-export:queue --all
   *   Queue all audit reports for processing.
   *
   * @group Audit Export
   */
  public function queueAudit($audit_id = NULL, array $options = ['all' => FALSE]) {
    // If --all flag is set, queue all audits.
    if ($options['all']) {
      $timeout = $this->configFactory->get('audit_export_core.settings')
        ->get('audit_export_cron_queue_timeout') ?: 120;
      $items_queued = $this->cronService->queueAudits($timeout);

      if ($items_queued > 0) {
        $this->logger()->success(
          sprintf('Queued %d audit items for processing.', $items_queued)
        );
      }
      else {
        $this->logger()->warning('No audit items were queued for processing.');
      }

      return;
    }

    // Validate a specific audit ID.
    if (!empty($audit_id)) {
      $plugin_definitions = $this->pluginManager->getDefinitions();

      if (!isset($plugin_definitions[$audit_id])) {
        $this->logger()->error(
          sprintf('Audit report "%s" does not exist.', $audit_id)
        );
        return;
      }

      // Create a single queue item for this audit.
      $queue = $this->queueFactory->get('audit_export_processor');
      $queue->createItem([
        'plugin_id' => $audit_id,
        'group' => $plugin_definitions[$audit_id]['group'] ?? 'general',
      ]);

      $this->logger()->success(
        sprintf('Audit report "%s" has been queued for processing.', $audit_id)
      );
    }
    else {
      // If no audit ID and no --all option, show an error.
      $this->logger()->error('Please specify an audit ID or use the --all option.');
    }
  }

  /**
   * Prints environment information for audit export.
   *
   * @command audit-export:env
   * @aliases aexenv
   * @usage audit-export:env
   *   Prints environment information.
   *
   * @group Audit Export
   */
  public function printEnv() {
    $info = [
      'PHP Version' => phpversion(),
      'Drupal Root' => DRUPAL_ROOT,
      'Drupal Version' => \constant('VERSION'),
      'Audit Export Version' => $this->getModuleVersion('audit_export_core'),
      'Server IP' => $_SERVER['SERVER_ADDR'] ?? 'Unknown',
      'Default Theme' => $this->configFactory->get('system.theme')->get('default'),
      'Admin Theme' => $this->configFactory->get('system.theme')->get('admin'),
      'Site Path' => $this->state->get('system.path_root', DRUPAL_ROOT),
    ];

    $rows = [];
    foreach ($info as $key => $value) {
      $rows[] = [$key, $value];
    }

    $this->io()->title('Audit Export Environment Information');
    $this->io()->table(['Property', 'Value'], $rows);
  }

  /**
   * Gets the version of a module.
   *
   * @param string $module_name
   *   The name of the module.
   *
   * @return string
   *   The module version or 'Unknown'.
   */
  protected function getModuleVersion($module_name) {
    $module_info = $this->moduleExtensionList->getExtensionInfo($module_name);
    return $module_info['version'] ?? 'Unknown';
  }

}
