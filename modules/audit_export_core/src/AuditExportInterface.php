<?php

declare(strict_types=1);

namespace Drupal\audit_export_core;

/**
 * Interface for audit_export plugins.
 */
interface AuditExportInterface {

  /**
   * Returns the translated plugin label.
   */
  public function label(): string;

  /**
   * Returns the module associated with the audit.
   */
  public function getModule(): string;

  /**
   * Returns the data type of the audit.
   */
  public function getDataType(): string;

  /**
   * Returns the unique identifier of the audit.
   */
  public function getIdentifier(): string;

  /**
   * Returns the group the audit belongs to.
   */
  public function getGroup(): string;

  /**
   * Returns the dependencies for the audit.
   */
  public function getDependencies(): array;

  /**
   * Returns the class name implementing the audit logic.
   */
  public function getClassName(): string;

}
