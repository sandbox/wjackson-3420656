<?php

namespace Drupal\audit_export_core;

/**
 * Defines the interface for Audit Export plugins.
 */
interface AuditExportPluginInterface {

  /**
   * Prepares data for the audit.
   *
   * @return array
   *   The data prepared for the audit.
   */
  public function prepareData(): array;

  /**
   * Processes data for the audit.
   *
   * @param array $params
   *   Parameters for processing the data.
   *
   * @return array
   *   The processed data.
   */
  public function processData(array $params): array;

  /**
   * Returns the label of the plugin.
   *
   * @return string
   *   The label of the plugin.
   */
  public function label(): string;

  /**
   * Returns the module associated with the audit.
   *
   * @return string
   *   The module name.
   */
  public function getModule(): string;

  /**
   * Returns the data type of the audit.
   *
   * @return string
   *   The data type (flat, cross, process).
   */
  public function getDataType(): string;

  /**
   * Returns the unique identifier of the audit.
   *
   * @return string
   *   The identifier field name.
   */
  public function getIdentifier(): string;

  /**
   * Returns the group the audit belongs to.
   *
   * @return string
   *   The group name.
   */
  public function getGroup(): string;

  /**
   * Returns the dependencies for the audit.
   *
   * @return array
   *   The dependencies.
   */
  public function getDependencies(): array;

  /**
   * Returns the class name implementing the audit logic.
   *
   * @return string
   *   The class name.
   */
  public function getClassName(): string;

  /**
   * Prepares cross-tab scripts for the audit.
   *
   * @return array
   *   The cross-tab scripts.
   */
  public function prepareCrossTabScripts(): array;

  /**
   * Prepares data pre-processing operations.
   *
   * @return array
   *   The pre-processing operations.
   */
  public function processDataPreProcess(): array;

  /**
   * Gets the report headers.
   *
   * @return array
   *   The headers for the report.
   */
  public function getHeaders(): array;

  /**
   * Sets the report headers.
   *
   * @param array $headers
   *   The headers for the report.
   */
  public function setHeaders(array $headers);

  /**
   * Gets the cross-tab header label.
   *
   * @return string
   *   The cross-tab header label.
   */
  public function getCrossTabHeaderLabel(): string;

  /**
   * Sets the cross-tab header label.
   *
   * @param string $crossTab_header_label
   *   The cross-tab header label.
   */
  public function setCrossTabHeaderLabel(string $crossTab_header_label);

}
