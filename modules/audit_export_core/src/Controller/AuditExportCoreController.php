<?php

namespace Drupal\audit_export_core\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\audit_export_core\Service\AuditExportAuditReport;

/**
 * Controller for Audit Export Core.
 */
class AuditExportCoreController extends ControllerBase {

  /**
   * The audit export plugin manager.
   *
   * @var \Drupal\Core\Plugin\DefaultPluginManager
   */
  protected $pluginManager;

  /**
   * The audit export report service.
   *
   * @var \Drupal\audit_export_core\Service\AuditExportAuditReport
   */
  protected $auditReportService;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new AuditExportCoreController object.
   *
   * @param \Drupal\Core\Plugin\DefaultPluginManager $plugin_manager
   *   The plugin manager for audit export plugins.
   * @param \Drupal\audit_export_core\Service\AuditExportAuditReport $audit_report_service
   *   The audit export report service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(
    DefaultPluginManager $plugin_manager,
    AuditExportAuditReport $audit_report_service,
    Connection $database,
    DateFormatterInterface $date_formatter,
    RequestStack $request_stack,
    LoggerChannelFactoryInterface $logger_factory,
    MessengerInterface $messenger,
    ModuleHandlerInterface $module_handler,
    StateInterface $state,
    ConfigFactoryInterface $config_factory,
  ) {
    $this->pluginManager = $plugin_manager;
    $this->auditReportService = $audit_report_service;
    $this->database = $database;
    $this->dateFormatter = $date_formatter;
    $this->requestStack = $request_stack;
    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;
    $this->moduleHandler = $module_handler;
    $this->state = $state;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.audit_export_audit'),
      $container->get('audit_export_core.audit_report'),
      $container->get('database'),
      $container->get('date.formatter'),
      $container->get('request_stack'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('module_handler'),
      $container->get('state'),
      $container->get('config.factory')
    );
  }

  /**
   * Shows an overview of all reports.
   *
   * @return array
   *   A renderable array containing the reports overview.
   */
  public function reportsOverview() {
    // Define the table headers.
    $header = ['Audit', 'Group', 'Description', 'Last Processed', 'Actions'];
    $rows = [];

    // Get all available plugin definitions from the plugin manager.
    $plugin_definitions = $this->pluginManager->getDefinitions();

    // Loop through each plugin definition to display in the overview.
    foreach ($plugin_definitions as $id => $definition) {
      // Retrieve the last processed date from the service.
      $last_processed = $this->auditReportService->getLastProcessedDate($id) ?? $this->t('Not processed yet');

      // Create links for the audit and group.
      $audit_link = Link::fromTextAndUrl(
        $definition['label'],
        Url::fromRoute(
          'audit_export_core.view_report', [
            'group' => $definition['group'],
            'id' => $id,
          ]
        )
      )->toString();

      $group_link = Link::fromTextAndUrl(
        $definition['group'],
        Url::fromRoute(
          'audit_export_core.view_group', [
            'group' => $definition['group'],
          ]
        )
      )->toString();

      // Create a link for the process action.
      $process_link = Link::fromTextAndUrl(
        $this->t('Run audit'),
        Url::fromRoute(
          'audit_export_core.process_audit', [
            'group' => $definition['group'],
            'audit_name' => $id,
          ]
        )
      )->toString();

      // Add the row data for the table.
      $rows[] = [
        'data' => [
          $audit_link,
          $group_link,
          $definition['description'],
          $last_processed,
          $process_link,
        ],
      ];
    }

    // Create the CTA link.
    $cta_link = Link::fromTextAndUrl(
      $this->t('Process all'),
      Url::fromRoute('audit_export_core.audit_export_process_all')
    )->toString();

    // Render the header section above the table.
    $header_section = [
      '#type' => 'markup',
      '#markup' => '<div class="cta-section button button--action">' . $cta_link . '</div>',
    ];

    // Render the table with the header and rows.
    $table = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No audits available.'),
    ];

    // Return the complete render array with the header section and the table.
    return [
      $header_section,
      $table,
    ];
  }

  /**
   * Displays the audits for a specific group.
   *
   * @param string $group
   *   The group name.
   *
   * @return array
   *   Render array for the group overview page.
   */
  public function viewGroup(string $group) {
    $audits = $this->getAudits();
    $header = ['Audit', 'Description', 'Last Processed', 'Actions'];
    $rows = [];

    foreach ($audits as $audit) {
      if ($audit['group'] === $group) {
        try {
          // Get the last processed date for this audit.
          $last_processed = $this->auditReportService->getLastProcessedDate($audit['id']) ?? $this->t('Not processed yet');

          // Create the audit name link.
          $audit_link = Link::fromTextAndUrl(
            $audit['label'],
            Url::fromRoute('audit_export_core.view_report', [
              'group' => $group,
              'id' => $audit['id'],
            ])
          )->toString();

          // Create the process link.
          $process_link = Link::fromTextAndUrl(
            $this->t('Run audit'),
            Url::fromRoute('audit_export_core.process_audit', [
              'group' => $group,
              'audit_name' => $audit['id'],
            ])
          )->toString();

          $rows[] = [
            'data' => [
              $audit_link,
              $audit['description'],
              $last_processed,
              $process_link,
            ],
          ];
        }
        catch (\Exception $e) {
          $this->loggerFactory->get('audit_export')->error('Error processing audit row: @message', ['@message' => $e->getMessage()]);
          continue;
        }
      }
    }

    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No audits available for this group.'),
      '#cache' => [
        'tags' => ['audit_export:group:' . $group],
        'contexts' => ['user.permissions'],
      ],
    ];
  }

  /**
   * Get the title for the group overview page.
   *
   * @param string $group
   *   The group name.
   *
   * @return string
   *   The title of the page.
   */
  public function getGroupTitle(string $group) {
    return $this->t('%group', ['%group' => ucfirst($group)]);
  }

  /**
   * Displays a specific audit report.
   *
   * @param string $group
   *   The group name.
   * @param string $id
   *   The plugin ID.
   *
   * @return array
   *   A renderable array.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function viewReport($group, $id) {
    // Retrieve the plugin instance using the plugin manager.
    $plugin = $this->pluginManager->createInstance($id);

    if (!$plugin) {
      throw new NotFoundHttpException();
    }

    // Retrieve the report data from the service.
    $processed_data = $this->auditReportService->getReportData($id);

    // Fetch the last run date from the audit_export_report table.
    $query = $this->database->select('audit_export_report', 'aer')
      ->fields('aer', ['date'])
      ->condition('audit', $id)
      ->execute()
      ->fetchField();

    // Format the last run date, defaulting to 'Never' if no date is found.
    $last_run_date = $query ? $this->dateFormatter->format($query, 'custom', 'm/d/Y - H:i') : $this->t('Never');

    // Create the CTA links container.
    $cta_links = [
      '#type' => 'container',
      '#attributes' => ['class' => ['cta-section']],
    ];

    // Add the process report link.
    $cta_links['process'] = [
      '#type' => 'link',
      '#title' => $this->t('Process Report'),
      '#url' => Url::fromRoute('audit_export_core.process_audit', [
        'group' => $group,
        'audit_name' => $id,
        'from_view_report' => '1',
      ]),
      '#attributes' => ['class' => ['button', 'button--action']],
    ];

    // Check if filesystem export is enabled.
    $config = $this->configFactory->get('audit_export_core.settings');
    if ($config->get('audit_export_save_filesystem')) {
      $cta_links['download'] = [
        '#type' => 'link',
        '#title' => $this->t('Download CSV'),
        '#url' => Url::fromRoute('audit_export_core.download_report', [
          'group' => $group,
          'id' => $id,
        ]),
        '#attributes' => ['class' => ['button', 'button--action', 'button--download']],
      ];
    }

    // Last updated text.
    $last_updated = [
      '#type' => 'markup',
      '#markup' => '<div class="last-updated">' . $this->t('Last updated: @date', ['@date' => $last_run_date]) . '</div>',
    ];

    // Render the table with the report data.
    $table = [
      '#theme' => 'table',
      '#header' => $plugin->getHeaders(),
      '#rows' => $processed_data,
      '#empty' => $this->t('No data available for this report.'),
    ];

    // Return the complete render array with the header section and the table.
    return [
      $cta_links,
      $last_updated,
      $table,
    ];
  }

  /**
   * Downloads the report as a CSV file.
   *
   * @param string $group
   *   The group name.
   * @param string $id
   *   The plugin ID.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The CSV file response.
   */
  public function downloadReport($group, $id) {
    // Check if filesystem export is enabled.
    $config = $this->configFactory->get('audit_export_core.settings');
    if (!$config->get('audit_export_save_filesystem')) {
      throw new AccessDeniedHttpException('File system export is not enabled.');
    }

    // Get the plugin instance.
    $plugin = $this->pluginManager->createInstance($id);
    if (!$plugin) {
      throw new NotFoundHttpException('Audit plugin not found.');
    }

    // Get the report data.
    $data = $this->auditReportService->getReportData($id);
    if (empty($data)) {
      throw new NotFoundHttpException('No report data available.');
    }

    // Create the CSV content.
    $handle = fopen('php://temp', 'r+');

    // Write headers.
    fputcsv($handle, $plugin->getHeaders());

    // Write data rows.
    foreach ($data as $row) {
      fputcsv($handle, $row);
    }

    // Get the content.
    rewind($handle);
    $csv_content = stream_get_contents($handle);
    fclose($handle);

    // Create the response.
    $response = new Response($csv_content);
    $response->headers->set('Content-Type', 'text/csv');
    $response->headers->set('Content-Disposition', 'attachment; filename="audit_report_' . $id . '_' . date('Y-m-d') . '.csv"');

    return $response;
  }

  /**
   * Process all audits using the Batch API.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response to the overview page.
   */
  public function processAll() {
    // Retrieve all audits from the plugin manager.
    $audits = $this->getAudits();

    if (empty($audits)) {
      $this->messenger->addMessage($this->t('No audits found to process.'));
      return $this->redirect('audit_export_core.audit_export_reports');
    }

    // Prepare batch operations for processing each audit.
    $batch = [
      'title' => $this->t('Processing All Audit Reports'),
      'operations' => [],
      'init_message' => $this->t('Starting processing of all audits...'),
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message' => $this->t('An error occurred during processing audits.'),
      'finished' => [get_class($this), 'batchFinishedCallback'],
    ];

    // Add an initialization operation to set the batch mode flag.
    $batch['operations'][] = [
      [get_class($this), 'setBatchMode'],
      // Setting batch_mode flag to TRUE.
      [TRUE],
    ];

    // Loop through each audit and set up batch operations.
    foreach ($audits as $audit) {
      $batch['operations'][] = [
        [get_class($this), 'processSingleAudit'],
        [['group' => $audit['group'], 'audit_name' => $audit['id']]],
      ];
    }

    // Set the batch.
    batch_set($batch);

    // Process the batch and redirect to the reports overview page.
    $redirect_url = Url::fromRoute('audit_export_core.audit_export_reports')->toString();
    return batch_process($redirect_url);
  }

  /**
   * Process a specific audit using the Batch API.
   *
   * @param string $group
   *   The group name.
   * @param string $audit_name
   *   The name of the audit to process.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response to the appropriate page.
   */
  public function processAudit($group, $audit_name) {
    // Get the specific audit plugin instance.
    $audit = $this->getAudit($audit_name);
    if (!$audit) {
      throw new NotFoundHttpException();
    }

    // Clear existing data for the audit to prepare for new processing.
    $this->auditReportService->clearReportData($audit_name);

    // Prepare batch operations.
    $batch = [
      'title' => $this->t('Processing Audit Report'),
      'operations' => [],
      'init_message' => $this->t('Starting audit processing...'),
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message' => $this->t('An error occurred during processing.'),
      'finished' => [get_class($this), 'batchFinishedCallback'],
    ];

    // Add an initialization operation to set the batch mode flag.
    $batch['operations'][] = [
      [get_class($this), 'setBatchMode'],
      // Setting batch_mode flag to FALSE for single audit.
      [FALSE],
    ];

    // Store metadata about this audit in the batch context.
    $batch['operations'][] = [
      [get_class($this), 'initializeAuditContext'],
      [$audit_name, $group],
    ];

    // Operations for cross-tab scripts if this is a cross-tab report.
    if ($audit->getDataType() == 'cross') {
      // Add cross-tab scripts to batch operations.
      if (method_exists($audit, 'prepareCrossTabScripts')) {
        $crossTabScripts = $audit->prepareCrossTabScripts();

        foreach ($crossTabScripts as $script_id => $script) {
          $batch['operations'][] = [
            [get_class($this), 'processCrossTabScript'],
            [
              [
                'script_id' => $script_id,
                'script' => $script,
                'audit_name' => $audit_name,
              ],
            ],
          ];
        }
      }

      // Add pre-process operations to batch operations.
      if (method_exists($audit, 'processDataPreProcess')) {
        $preProcessOperations = $audit->processDataPreProcess();

        foreach ($preProcessOperations as $operation) {
          $batch['operations'][] = [
            [get_class($this), 'processPreProcessOperation'],
            [
              [
                'operation' => $operation,
                'audit_name' => $audit_name,
              ],
            ],
          ];
        }
      }
    }

    // Add operations for processing the main report data.
    $report_data = $audit->prepareData();
    foreach ($report_data as $data) {
      $batch['operations'][] = [
        [get_class($this), 'processBatchOperation'],
        [['data' => $data, 'audit' => $audit_name]],
      ];
    }

    // Check if the request came from the viewReport page.
    $from_view_report = $this->requestStack->getCurrentRequest()->query->get('from_view_report', FALSE);

    // Store the from_view_report flag in the context.
    $batch['operations'][] = [
      [get_class($this), 'setViewReportFlag'],
      [$from_view_report],
    ];

    // Determine the redirect URL based on the originating route.
    $redirect_url = $from_view_report
      ? Url::fromRoute('audit_export_core.view_report', ['group' => $group, 'id' => $audit_name])->toString()
      : Url::fromRoute('audit_export_core.audit_export_reports')->toString();

    // Set the batch.
    batch_set($batch);

    // Process the batch and redirect to the determined URL.
    return batch_process($redirect_url);
  }

  /**
   * Initialize the audit context in batch processing.
   *
   * @param string $audit_name
   *   The audit name.
   * @param string $group
   *   The group name.
   * @param array $context
   *   The batch context.
   */
  public static function initializeAuditContext($audit_name, $group, &$context) {
    if (!isset($context['results']['audits'][$audit_name])) {
      $context['results']['audits'][$audit_name] = [
        'group' => $group,
        'processed_items' => 0,
        'success' => TRUE,
        'data' => [],
      ];
    }

    $context['results']['current_audit'] = $audit_name;
    $context['message'] = t('Initializing audit processing for @name...', ['@name' => $audit_name]);
  }

  /**
   * Sets the from_view_report flag in the batch context.
   *
   * @param bool $from_view_report
   *   Whether the process was initiated from the viewReport route.
   * @param array $context
   *   The batch context.
   */
  public static function setViewReportFlag($from_view_report, &$context) {
    $context['results']['from_view_report'] = $from_view_report;
    $context['message'] = t('Setting view report flag...');
  }

  /**
   * Gets the title for the report view page.
   *
   * @param string $group
   *   The group name.
   * @param string $id
   *   The plugin ID.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   The report title.
   */
  public function getReportTitle($group, $id) {
    try {
      // Get the plugin definition to access the label.
      $definitions = $this->pluginManager->getDefinitions();
      if (isset($definitions[$id])) {
        return $definitions[$id]['label'];
      }
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('audit_export')->error('Error getting report title: @message', ['@message' => $e->getMessage()]);
    }

    // Fallback title if plugin not found.
    return $this->t('Report Details');
  }

  /**
   * Sets the batch mode flag in the batch context.
   *
   * @param bool $mode
   *   TRUE for batch mode, FALSE for single audit.
   * @param array $context
   *   The batch context.
   */
  public static function setBatchMode($mode, &$context) {
    $context['results']['batch_mode'] = $mode;
    $context['results']['audits'] = [];
    $context['message'] = t('Initializing batch processing mode...');
  }

  /**
   * Batch operation callback to process a single audit.
   *
   * @param array $item
   *   Contains 'group' and 'audit_name' for the audit.
   * @param array $context
   *   The batch context array.
   */
  public static function processSingleAudit($item, &$context) {
    // Get the container and required services for this static method.
    $container = \Drupal::getContainer();
    $plugin_manager = $container->get('plugin.manager.audit_export_audit');
    $audit_service = $container->get('audit_export_core.audit_report');
    $logger = $container->get('logger.factory')->get('audit_export');
    $messenger = $container->get('messenger');

    $group = $item['group'];
    $audit_name = $item['audit_name'];

    // Get the specific audit plugin instance.
    $audit = $plugin_manager->createInstance($audit_name);
    if (!$audit) {
      $messenger->addError(t('Audit plugin not found: @name', ['@name' => $audit_name]));
      return;
    }

    // Create a context for this specific audit.
    if (!isset($context['results']['audits'][$audit_name])) {
      $context['results']['audits'][$audit_name] = [
        'group' => $group,
        'processed_items' => 0,
        'success' => TRUE,
        'data' => [],
      ];
    }

    // Clear existing data for the audit to prepare for new processing.
    $audit_service->clearReportData($audit_name);

    // Handle cross-tab report types.
    if ($audit->getDataType() == 'cross') {
      // Process cross-tab scripts.
      if (method_exists($audit, 'prepareCrossTabScripts')) {
        $crossTabScripts = $audit->prepareCrossTabScripts();

        foreach ($crossTabScripts as $script_id => $script) {
          if (method_exists($audit, $script['method'])) {
            $logger->notice('Processing cross-tab script: @script for @audit', [
              '@script' => $script_id,
              '@audit' => $audit_name,
            ]);

            // Call the script method with the script data as parameter.
            $script_result = call_user_func([$audit, $script['method']], $script);

            // Save the script result to the report.
            $audit_service->appendReportData($audit_name, $script_result);

            // Store in the context results.
            $context['results']['audits'][$audit_name]['cross_tab_data'][$script_id] = $script_result;
            $context['message'] = t('Processing cross-tab script @script...', ['@script' => $script_id]);
          }
          else {
            $logger->error('Cross-tab script method @method not found in @audit', [
              '@method' => $script['method'],
              '@audit' => $audit_name,
            ]);
          }
        }
      }

      // Process pre-process operations.
      if (method_exists($audit, 'processDataPreProcess')) {
        $preProcessOperations = $audit->processDataPreProcess();

        foreach ($preProcessOperations as $operation) {
          if (isset($operation['method']) && method_exists($audit, $operation['method'])) {
            $logger->notice('Processing pre-process operation: @operation for @audit', [
              '@operation' => $operation['name'],
              '@audit' => $audit_name,
            ]);

            // Call the pre-process method.
            $preprocess_result = call_user_func([$audit, $operation['method']]);

            // Save the pre-process result to the report.
            $audit_service->appendReportData($audit_name, $preprocess_result);

            // Store in the context results.
            $context['results']['audits'][$audit_name]['preprocess_data'][$operation['name']] = $preprocess_result;
            $context['message'] = t('Processing pre-process operation @operation...', ['@operation' => $operation['name']]);
          }
        }
      }
    }

    // Prepare the data to be processed by the batch.
    $report_data = $audit->prepareData();
    foreach ($report_data as $data) {
      // Process each data item.
      try {
        // Process data using the audit plugin's processData method.
        $processed_data = $audit->processData(['row_data' => $data]);

        // Append processed data to the report.
        $audit_service->appendReportData($audit_name, $processed_data);

        // Store results in the audit-specific context.
        $context['results']['audits'][$audit_name]['data'][] = $processed_data;
        $context['results']['audits'][$audit_name]['processed_items']++;

        $context['message'] = t('Processing audit @name...', ['@name' => $audit_name]);
      }
      catch (\Exception $e) {
        // Log and report any errors encountered during processing.
        $logger->error('Error processing @audit: @message', [
          '@audit' => $audit_name,
          '@message' => $e->getMessage(),
        ]);
        $context['message'] = t('Error processing audit @name: @message', [
          '@name' => $audit_name,
          '@message' => $e->getMessage(),
        ]);

        // Mark this audit as failed.
        $context['results']['audits'][$audit_name]['success'] = FALSE;
      }
    }

    // Update report date right after processing this audit.
    $audit_service->updateReportDate($audit_name);
  }

  /**
   * Batch operation callback to process individual audit data.
   *
   * @param array $item
   *   The data item to process.
   * @param array $context
   *   The batch context array.
   */
  public static function processBatchOperation($item, &$context) {
    // Get the container and services for this static method.
    $container = \Drupal::getContainer();
    $plugin_manager = $container->get('plugin.manager.audit_export_audit');
    $audit_service = $container->get('audit_export_core.audit_report');
    $logger = $container->get('logger.factory')->get('audit_export');
    $messenger = $container->get('messenger');

    $audit_name = $item['audit'];
    $data = $item['data'];

    // Retrieve the audit plugin instance using the plugin manager service.
    $audit = $plugin_manager->createInstance($audit_name);

    if (!$audit) {
      // Handle the case where the plugin cannot be instantiated.
      $messenger->addError(t('Unable to load audit plugin: @name', ['@name' => $audit_name]));
      return;
    }

    try {
      // Process data using the plugin's processData method.
      $processed_data = $audit->processData(['row_data' => $data]);

      // Append the processed data to the existing report in the database.
      $audit_service->appendReportData($audit_name, $processed_data);

      // Store results in the audit-specific context.
      if (!isset($context['results']['audits'][$audit_name])) {
        $context['results']['audits'][$audit_name] = [
          'processed_items' => 0,
          'success' => TRUE,
          'data' => [],
        ];
      }

      $context['results']['audits'][$audit_name]['data'][] = $processed_data;
      $context['results']['audits'][$audit_name]['processed_items']++;

      $context['message'] = t('Processing @label...', ['@label' => $data['label'] ?? 'Unnamed item']);
    }
    catch (\Exception $e) {
      // Log the error and add a message to the context.
      $logger->error(
        'Error processing @audit: @message', [
          '@audit' => $audit_name,
          '@message' => $e->getMessage(),
        ]
      );
      $context['message'] = t(
        'Error processing @label: @message', [
          '@label' => $data['label'] ?? 'Unnamed item',
          '@message' => $e->getMessage(),
        ]
      );

      // Mark this audit as failed.
      $context['results']['audits'][$audit_name]['success'] = FALSE;
    }
  }

  /**
   * Batch finished callback for processing audits.
   *
   * @param bool $success
   *   TRUE if the batch processing was successful.
   * @param array $results
   *   The results array.
   * @param array $operations
   *   The batch operations array.
   */
  public static function batchFinishedCallback($success, array $results, array $operations) {
    // Get the container and services for this static method.
    $container = \Drupal::getContainer();
    $messenger = $container->get('messenger');
    $module_handler = $container->get('module_handler');

    $batch_mode = $results['batch_mode'] ?? FALSE;

    // Process batch mode results (multiple audits)
    if ($batch_mode) {
      if ($success) {
        if (!empty($results['audits'])) {
          foreach ($results['audits'] as $audit_name => $audit_result) {
            if ($audit_result['success']) {
              $messenger->addMessage(t('@audit_name completed successfully.', ['@audit_name' => $audit_name]));
            }
            else {
              $messenger->addError(t('@audit_name encountered errors.', ['@audit_name' => $audit_name]));
            }
          }

          // Notify modules that all audits are processed (batch mode)
          $module_handler->invokeAll('audit_export_batch_complete', [$results['audits']]);
        }
      }
      else {
        $messenger->addError(t('Batch processing encountered errors.'));
      }
    }
    else {
      // Process single audit mode.
      $current_audit = $results['current_audit'] ?? '';

      if (!empty($current_audit) && isset($results['audits'][$current_audit])) {
        $audit_result = $results['audits'][$current_audit];
        $group = $audit_result['group'] ?? '';
        $from_view_report = $results['from_view_report'] ?? FALSE;

        if ($success && $audit_result['success']) {
          $messenger->addMessage(t('@audit_name completed successfully.', ['@audit_name' => $current_audit]));

          // Notify modules that a single audit finished successfully.
          $module_handler->invokeAll('audit_export_audit_finished', [
            $current_audit,
            ['data' => $audit_result['data']],
            TRUE,
          ]);
        }
        else {
          $messenger->addError(t('@audit_name encountered errors.', ['@audit_name' => $current_audit]));

          // Notify modules that a single audit finished with errors.
          $module_handler->invokeAll('audit_export_audit_finished', [
            $current_audit,
            ['data' => $audit_result['data'] ?? []],
            FALSE,
          ]);
        }

        // Redirect to the appropriate page based on the originating route.
        if ($from_view_report && $group) {
          $response = new RedirectResponse(
            Url::fromRoute('audit_export_core.view_report', ['group' => $group, 'id' => $current_audit])->toString()
          );
          $response->send();
        }
      }
      else {
        $messenger->addError(t('Audit processing completed with errors.'));
      }
    }
  }

  /**
   * Process a cross-tab script in the batch operation.
   *
   * @param array $item
   *   Contains script information and audit name.
   * @param array $context
   *   The batch context array.
   */
  public static function processCrossTabScript($item, &$context) {
    // Get the container and services for this static method.
    $container = \Drupal::getContainer();
    $plugin_manager = $container->get('plugin.manager.audit_export_audit');
    $audit_service = $container->get('audit_export_core.audit_report');
    $logger = $container->get('logger.factory')->get('audit_export');

    $script_id = $item['script_id'];
    $script = $item['script'];
    $audit_name = $item['audit_name'];

    // Load the audit plugin.
    $audit = $plugin_manager->createInstance($audit_name);
    if (!$audit) {
      $logger->error('Unable to load audit plugin for cross-tab script: @name', [
        '@name' => $audit_name,
      ]);
      return;
    }

    // Check if the method exists on the audit plugin.
    if (method_exists($audit, $script['method'])) {
      $logger->notice('Processing cross-tab script: @script for @audit', [
        '@script' => $script_id,
        '@audit' => $audit_name,
      ]);

      // Call the script method with the script data as parameter.
      $script_result = call_user_func([$audit, $script['method']], $script);

      // Save the script result to the report.
      $audit_service->appendReportData($audit_name, $script_result);

      // Store in the context results.
      if (!isset($context['results']['audits'][$audit_name])) {
        $context['results']['audits'][$audit_name] = [
          'processed_items' => 0,
          'success' => TRUE,
          'data' => [],
          'cross_tab_data' => [],
        ];
      }

      $context['results']['audits'][$audit_name]['cross_tab_data'][$script_id] = $script_result;
      $context['message'] = t('Processing cross-tab script @script...', ['@script' => $script_id]);
    }
    else {
      $logger->error('Cross-tab script method @method not found in @audit', [
        '@method' => $script['method'],
        '@audit' => $audit_name,
      ]);
    }
  }

  /**
   * Process a pre-process operation in the batch operation.
   *
   * @param array $item
   *   Contains operation information and audit name.
   * @param array $context
   *   The batch context array.
   */
  public static function processPreProcessOperation($item, &$context) {
    // Get the container and services for this static method.
    $container = \Drupal::getContainer();
    $plugin_manager = $container->get('plugin.manager.audit_export_audit');
    $audit_service = $container->get('audit_export_core.audit_report');
    $logger = $container->get('logger.factory')->get('audit_export');

    $operation = $item['operation'];
    $audit_name = $item['audit_name'];

    // Load the audit plugin.
    $audit = $plugin_manager->createInstance($audit_name);
    if (!$audit) {
      $logger->error('Unable to load audit plugin for pre-process operation: @name', [
        '@name' => $audit_name,
      ]);
      return;
    }

    // Check if the method exists on the audit plugin.
    if (isset($operation['method']) && method_exists($audit, $operation['method'])) {
      $logger->notice('Processing pre-process operation: @operation for @audit', [
        '@operation' => $operation['name'],
        '@audit' => $audit_name,
      ]);

      // Call the pre-process method.
      $preprocess_result = call_user_func([$audit, $operation['method']]);

      // Save the pre-process result to the report.
      $audit_service->appendReportData($audit_name, $preprocess_result);

      // Store in the context results.
      if (!isset($context['results']['audits'][$audit_name])) {
        $context['results']['audits'][$audit_name] = [
          'processed_items' => 0,
          'success' => TRUE,
          'data' => [],
          'preprocess_data' => [],
        ];
      }

      $context['results']['audits'][$audit_name]['preprocess_data'][$operation['name']] = $preprocess_result;
      $context['message'] = t('Processing pre-process operation @operation...', ['@operation' => $operation['name']]);
    }
    else {
      $logger->error('Pre-process method @method not found in @audit', [
        '@method' => $operation['method'],
        '@audit' => $audit_name,
      ]);
    }
  }

  /**
   * Stores the audit name in the batch context for the finished callback.
   *
   * @param string $audit_name
   *   The name of the audit.
   * @param array $context
   *   The batch context array.
   */
  public static function storeAuditNameInContext($audit_name, &$context) {
    $context['results']['audit_name'] = $audit_name;
  }

  /**
   * Stores the audit name, originating route, and group in the batch context.
   *
   * @param string $audit_name
   *   The name of the audit.
   * @param bool $from_view_report
   *   Whether the process was initiated from the viewReport route.
   * @param string $group
   *   The group name.
   * @param array $context
   *   The batch context array.
   */
  public static function storeAuditContext($audit_name, $from_view_report, $group, &$context) {
    $context['results']['audit_name'] = $audit_name;
    $context['results']['from_view_report'] = $from_view_report;
    $context['results']['group'] = $group;
  }

  /**
   * Batch finished callback.
   *
   * @param bool $success
   *   TRUE if the batch processing was successful.
   * @param array $results
   *   The results array.
   * @param array $operations
   *   The batch operations array.
   */
  public static function auditReportFinished($success, array $results, array $operations) {
    // Get the container and services for this static method.
    $container = \Drupal::getContainer();
    $messenger = $container->get('messenger');

    if ($success) {
      $messenger->addMessage(t('Audit report processing completed successfully.'));
    }
    else {
      $messenger->addError(t('Audit report processing encountered errors.'));
    }
  }

  /**
   * Load all audits using the plugin manager.
   *
   * @return array
   *   An array of audit plugins.
   */
  protected function getAudits(): array {
    $plugin_definitions = $this->pluginManager->getDefinitions();
    $audits = [];

    foreach ($plugin_definitions as $plugin_id => $plugin_definition) {
      $audits[] = [
        'id' => $plugin_id,
        'label' => $plugin_definition['label'],
        'description' => $plugin_definition['description'] ?? '',
        'group' => $plugin_definition['group'] ?? 'general',
        'last_processed' => NULL,
      ];
    }

    return $audits;
  }

  /**
   * Get a specific audit plugin by its ID.
   *
   * @param string $audit_name
   *   The ID of the audit plugin to load.
   *
   * @return object|null
   *   The plugin instance, or null if not found.
   */
  protected function getAudit($audit_name) {
    try {
      return $this->pluginManager->createInstance($audit_name);
    }
    catch (\Exception $e) {
      $this->messenger->addError($this->t('Audit not found.'));
      return NULL;
    }
  }

}
