<?php

namespace Drupal\audit_export_core\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for Audit Export module settings.
 *
 * Provides config options for file exports, cron settings, and queue timeout.
 */
class AuditExportConfigForm extends ConfigFormBase {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a new AuditExportConfigForm object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   */
  public function __construct(FileSystemInterface $file_system) {
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['audit_export_core.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'audit_export_core_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('audit_export_core.settings');

    // Start with only the Public filesystem option.
    $options = [
      'temporary' => $this->t('Temporary'),
      'public' => $this->t('Public'),
    ];

    // Add the Private filesystem option only if it's configured.
    if ($this->fileSystem->realpath('private://')) {
      $options['private'] = $this->t('Private');
    }

    $form['report_file_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('CSV Download'),
      '#collapsible' => TRUE,
      '#collapsed' => !(bool) $config->get('audit_export_save_filesystem'),
    ];

    $form['report_file_settings']['save_to_filesystem'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Save export to filesystem'),
      '#default_value' => $config->get('audit_export_save_filesystem'),
      '#description' => $this->t('Enable this option to save exports to the filesystem.'),
    ];

    $form['report_file_settings']['audit_export_filesystem'] = [
      '#type' => 'radios',
      '#title' => $this->t('Filesystem'),
      '#options' => $options,
      '#default_value' => $config->get('audit_export_filesystem') ?: 'temporary',
      '#description' => $this->t('Select where the export files should be stored. Using the private filesystem is recommended if available.'),
      '#states' => [
        'enabled' => [
          ':input[name="save_to_filesystem"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $filesystem = $config->get('audit_export_filesystem');

    $form['report_file_settings']['audit_export_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Export path'),
      '#size' => 40,
      '#field_prefix' => (!empty($filesystem)) ? $filesystem . '://' : NULL,
      '#default_value' => $config->get('audit_export_filesystem_path') ?: 'audit-export',
      '#description' => $this->t('Enter the path where audit exports will be stored on the filesystem.'),
      '#states' => [
        'enabled' => [
          ':input[name="save_to_filesystem"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['cron_configuration'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Cron Configuration'),
      '#collapsible' => TRUE,
      '#collapsed' => !(bool) $config->get('audit_export_enable_cron'),
    ];

    $form['cron_configuration']['enable_report_cron'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable report processing on cron'),
      '#default_value' => $config->get('audit_export_enable_cron'),
      '#description' => $this->t('Enable this option to allow reports to be processed on cron.'),
    ];

    $form['cron_configuration']['run_on_every_cron'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Run on every cron'),
      '#default_value' => $config->get('audit_export_run_on_every_cron'),
      '#description' => $this->t('If enabled, the audit export will run on every cron execution (unless already processing). If disabled, it will use the frequency setting below.'),
      '#states' => [
        'enabled' => [
          ':input[name="enable_report_cron"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['cron_configuration']['default_frequency'] = [
      '#type' => 'number',
      '#title' => $this->t('Default frequency'),
      '#default_value' => $config->get('audit_export_cron_frequency_default') ?: 1440,
      '#size' => 15,
      '#description' => $this->t('The number of minutes that must pass before a report is refreshed. Only used if "Run on every cron" is disabled.'),
      '#states' => [
        'enabled' => [
          ':input[name="enable_report_cron"]' => ['checked' => TRUE],
          ':input[name="run_on_every_cron"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['cron_configuration']['queue_timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Queue timeout'),
      '#default_value' => $config->get('audit_export_cron_queue_timeout') ?: 120,
      '#size' => 15,
      '#description' => $this->t('When reports are refreshed on cron, they use the Queue API. What value would you like to set for the timeout for the queue?'),
      '#states' => [
        'enabled' => [
          ':input[name="enable_report_cron"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('audit_export_core.settings')
      ->set('audit_export_save_filesystem', $form_state->getValue('save_to_filesystem'))
      ->set('audit_export_filesystem', $form_state->getValue('audit_export_filesystem'))
      ->set('audit_export_filesystem_path', $form_state->getValue('audit_export_path'))
      ->set('audit_export_enable_cron', $form_state->getValue('enable_report_cron'))
      ->set('audit_export_run_on_every_cron', $form_state->getValue('run_on_every_cron'))
      ->set('audit_export_cron_frequency_default', $form_state->getValue('default_frequency'))
      ->set('audit_export_cron_queue_timeout', $form_state->getValue('queue_timeout'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
