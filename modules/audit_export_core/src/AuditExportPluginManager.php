<?php

namespace Drupal\audit_export_core;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages discovery and instantiation of audit export plugins.
 */
class AuditExportPluginManager extends DefaultPluginManager {

  /**
   * Constructs a new AuditExportPluginManager.
   *
   * @param \Traversable $namespaces
   *   An object that contains root paths keyed by the corresponding namespace
   *   to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    // Use custom annotation class.
    parent::__construct(
          'Plugin/AuditExport',
          $namespaces,
          $module_handler,
          'Drupal\audit_export_core\AuditExportPluginInterface',
          'Drupal\audit_export_core\Annotation\AuditExport'
      );

    $this->alterInfo('audit_export_info');
    $this->setCacheBackend($cache_backend, 'audit_export_plugins');
  }

}
