<?php

namespace Drupal\audit_export_core;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for audit export plugins.
 *
 * @phpstan-consistent-constructor
 */
abstract class AuditExportPluginBase extends PluginBase implements AuditExportPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The headers for the audit data export.
   *
   * @var array
   */
  protected $dataHeaders = [];

  /**
   * The cross-tab header label.
   *
   * @var string
   */
  protected $crossTabHeaderLabel = '';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructs a new AuditExportPluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface|null $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(
    array $configuration,
          $plugin_id,
          $plugin_definition,
    ?EntityTypeManagerInterface $entity_type_manager = NULL,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Sets the headers for the audit data export.
   *
   * @param array $headers
   *   An array of headers for the report.
   */
  public function setHeaders(array $headers) {
    $this->dataHeaders = $headers;
  }

  /**
   * Get the headers for the report.
   *
   * Includes cross-tab header label if available.
   *
   * @return array
   *   The headers for the report.
   */
  public function getHeaders(): array {
    $headers = [];
    if (!empty($this->getCrossTabHeaderLabel())) {
      $headers[] = $this->getCrossTabHeaderLabel();
    }
    return array_merge($headers, $this->dataHeaders);
  }

  /**
   * Set the cross-tab header label for the report.
   *
   * @param string $crossTabHeaderLabel
   *   The cross-tab header label.
   */
  public function setCrossTabHeaderLabel(string $crossTabHeaderLabel) {
    $this->crossTabHeaderLabel = $crossTabHeaderLabel;
  }

  /**
   * Get the cross-tab header label.
   *
   * @return string
   *   The cross-tab header label.
   */
  public function getCrossTabHeaderLabel(): string {
    return $this->crossTabHeaderLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getModule(): string {
    return isset($this->pluginDefinition['module'])
      ? (string) $this->pluginDefinition['module']
      : (string) ($this->pluginDefinition['provider'] ?? '');
  }

  /**
   * {@inheritdoc}
   */
  public function getDataType(): string {
    return isset($this->pluginDefinition['data_type'])
      ? (string) $this->pluginDefinition['data_type']
      : 'flat';
  }

  /**
   * {@inheritdoc}
   */
  public function getIdentifier(): string {
    return isset($this->pluginDefinition['identifier'])
      ? (string) $this->pluginDefinition['identifier']
      : 'id';
  }

  /**
   * {@inheritdoc}
   */
  public function getGroup(): string {
    return isset($this->pluginDefinition['group'])
      ? (string) $this->pluginDefinition['group']
      : 'general';
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(): array {
    return $this->pluginDefinition['dependencies'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getClassName(): string {
    return get_class($this);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareCrossTabScripts(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function processDataPreProcess(): array {
    return [];
  }

}
