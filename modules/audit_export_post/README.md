# Audit Export Post

The Audit Export Post module extends the Audit Export system to post audit data
to external services or endpoints. This enables integration with external
monitoring systems or centralized reporting tools.

## Features

- **Remote Posting**: Automatically send audit data to external endpoints when
reports are generated.
- **Flexible Authentication**: Supports multiple authentication methods
including none, basic auth, and bearer token.
- **Configurable Options**: Control timeout settings, SSL verification,
and other connection parameters.
- **Extensible API**: Hook into the posting process to customize data or
connection options.

## Use Cases

- **Centralized Monitoring**: Push audit data to centralized monitoring systems
to track multiple Drupal sites from a single location.
- **Integration with Analytics**: Send site audit data to analytics platforms
for further processing and visualization.
- **External Storage**: Store audit history on external systems for long-term
reporting and trend analysis.
- **Automated Workflows**: Trigger external workflows based on audit results
by posting data to workflow management systems.

## Requirements

- Audit Export Core module
- Working external endpoint to receive the posted data

## Installation

1. Install the Audit Export Post module alongside the Audit Export Core module:

```bash
composer require drupal/audit_export
```

2. Enable the module:

```bash
drush en audit_export_post
```

## Configuration

1. Navigate to the Audit Export settings at
`/admin/config/system/audit-export`.
2. Expand the "Remote Post" section.
3. Check "Enable remote post" to activate the functionality.
4. Enter the remote URL where audit data should be posted.
5. Configure authentication settings if required by your endpoint.
6. Configure advanced settings for timeout and SSL verification if needed.

## API

This module provides several hooks to extend and customize the posting
functionality:

- `hook_audit_export_post_url_alter(&$url)`: Alter the URL before posting.
- `hook_audit_export_post_site_info_alter(&$site_info)`: Modify the site
information sent with the data.
- `hook_audit_export_post_data_alter(&$data, $audit_name)`: Alter the full
data payload before sending.
- `hook_audit_export_post_request_options_alter(&$options, $audit_name)`:
Modify the request options for the HTTP client.

See the `audit_export_post.api.php` file for detailed documentation
and examples.

## Maintainers

- Will Jackson - Module Maintainer

## License

This project is licensed under the GNU General Public License, version 2
or later.
