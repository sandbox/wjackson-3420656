<?php

/**
 * @file
 * Hooks provided by the Audit Export Post module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Allows modules to alter the remote post URL.
 *
 * @param string $url
 *   The remote post URL.
 */
function hook_audit_export_post_url_alter(&$url) {
  // Add a query parameter to the URL.
  $url = $url . '?source=drupal';
}

/**
 * Allows modules to alter the site info sent with the post data.
 *
 * @param array $site_info
 *   The site info array.
 */
function hook_audit_export_post_site_info_alter(&$site_info) {
  // Add additional site info.
  $site_info['environment'] = 'production';
}

/**
 * Allows modules to alter the post data before it is sent.
 *
 * @param array $data
 *   The post data.
 * @param string $audit_name
 *   The name of the audit.
 */
function hook_audit_export_post_data_alter(&$data, $audit_name) {
  // Add custom data for a specific audit.
  if ($audit_name == 'content_type_audit') {
    $data['custom_data'] = [
      'last_content_update' => \Drupal::time()->getRequestTime(),
    ];
  }
}

/**
 * Allows modules to alter the request options before the request is sent.
 *
 * @param array $options
 *   The request options.
 * @param string $audit_name
 *   The name of the audit.
 */
function hook_audit_export_post_request_options_alter(&$options, $audit_name) {
  // Add custom headers for all requests.
  $options['headers']['X-Custom-Header'] = 'Custom Value';

  // Add specific options for a particular audit.
  if ($audit_name == 'site_report') {
    $options['headers']['X-Priority'] = 'High';
  }
}

/**
 * @} End of "addtogroup hooks".
 */
