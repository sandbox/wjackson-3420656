<?php

namespace Drupal\audit_export_core\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that is fired when an audit export process is complete.
 */
class AuditExportProcessCompleteEvent extends Event {

  /**
   * The event name.
   */
  const EVENT_NAME = 'audit_export_core.process_complete';

  /**
   * The audit name.
   *
   * @var string
   */
  protected $auditName;

  /**
   * The audit data.
   *
   * @var array
   */
  protected $data;

  /**
   * Constructs a new AuditExportProcessCompleteEvent.
   *
   * @param string $audit_name
   *   The audit name.
   * @param array $data
   *   The audit data.
   */
  public function __construct(string $audit_name, array $data) {
    $this->auditName = $audit_name;
    $this->data = $data;
  }

  /**
   * Gets the audit name.
   *
   * @return string
   *   The audit name.
   */
  public function getAuditName() {
    return $this->auditName;
  }

  /**
   * Gets the audit data.
   *
   * @return array
   *   The audit data.
   */
  public function getData() {
    return $this->data;
  }

}
