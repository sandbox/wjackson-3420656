<?php

namespace Drupal\audit_export_post\EventSubscriber;

use Drupal\audit_export_core\Event\AuditExportQueueCompleteEvent;
use Drupal\audit_export_core\Service\AuditExportAuditReport;
use Drupal\audit_export_post\Service\AuditExportRemotePost;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Component\Datetime\TimeInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber for Audit Export events.
 */
class AuditExportPostSubscriber implements EventSubscriberInterface {

  /**
   * The remote post service.
   *
   * @var \Drupal\audit_export_post\Service\AuditExportRemotePost
   */
  protected $remotePost;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The audit report service.
   *
   * @var \Drupal\audit_export_core\Service\AuditExportAuditReport
   */
  protected $auditReport;

  /**
   * Constructs a new AuditExportPostSubscriber.
   *
   * @param \Drupal\audit_export_post\Service\AuditExportRemotePost $remote_post
   *   The remote post service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\audit_export_core\Service\AuditExportAuditReport $audit_report
   *   The audit report service.
   */
  public function __construct(
    AuditExportRemotePost $remote_post,
    ConfigFactoryInterface $config_factory,
    Connection $database,
    LoggerChannelFactoryInterface $logger_factory,
    StateInterface $state,
    TimeInterface $time,
    AuditExportAuditReport $audit_report,
  ) {
    $this->remotePost = $remote_post;
    $this->configFactory = $config_factory;
    $this->database = $database;
    $this->loggerFactory = $logger_factory;
    $this->state = $state;
    $this->time = $time;
    $this->auditReport = $audit_report;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'cron' => ['onCron', 100],
      'audit_export_core.queue_complete' => ['onQueueComplete'],
    ];
  }

  /**
   * Check for updated audit reports during cron run.
   */
  public function onCron() {
    // Add verbose logging.
    $logger = $this->loggerFactory->get('audit_export_post');
    $logger->debug('AuditExportPostSubscriber::onCron called');

    // Skip if remote posting is not enabled.
    if (!$this->configFactory->get('audit_export_post.settings')->get('enable_remote_post')) {
      $logger->debug('Remote posting is disabled. Skipping cron processing.');
      return;
    }

    $logger->debug('Remote posting is enabled. Continuing with cron processing.');

    // Get reports that were updated since the last post.
    $last_post_time = $this->state->get('audit_export_post.last_post_time', 0);
    $current_time = $this->time->getRequestTime();

    $logger->debug('Last post time: @time', ['@time' => $last_post_time]);

    try {
      // Query for reports updated since the last post.
      $query = $this->database->select('audit_export_report', 'aer')
        ->fields('aer', ['audit', 'data', 'date'])
        ->condition('date', $last_post_time, '>')
        ->execute();

      // Collect results into an array - don't use rowCount()
      $results = $query->fetchAll();
      $count = count($results);

      $logger->debug('Found @count reports updated since last post', ['@count' => $count]);

      if ($count == 0) {
        $logger->debug('No new reports to process during cron');
        return;
      }

      $processed_count = 0;
      foreach ($results as $record) {
        // Load the data and post it.
        $audit_name = $record->audit;
        $logger->debug('Processing audit: @audit', ['@audit' => $audit_name]);

        // Get data directly from the audit report service to ensure fresh data.
        $data = $this->auditReport->getReportData($audit_name);

        if (empty($data)) {
          $logger->warning('No data found for audit @audit using auditReport service', ['@audit' => $audit_name]);

          // Try to decode data from the database record as fallback.
          $data = json_decode($record->data, TRUE);

          // If JSON decode fails, try unserialize with safety precautions.
          if ($data === NULL && is_string($record->data)) {
            $data = unserialize($record->data, ['allowed_classes' => FALSE]);
          }

          if (empty($data)) {
            $logger->error('Could not retrieve data for audit @audit', ['@audit' => $audit_name]);
            continue;
          }
        }

        // Post the data to the remote endpoint.
        $logger->debug('Posting data for audit @audit', ['@audit' => $audit_name]);
        $result = $this->remotePost->postData($audit_name, $data);

        if ($result) {
          $processed_count++;
          $logger->info('Successfully posted audit @audit during cron', ['@audit' => $audit_name]);
        }
        else {
          $logger->warning('Failed to post audit @audit during cron', ['@audit' => $audit_name]);
        }
      }

      // Update the last post time to avoid repeated attempts.
      $logger->debug('Setting last post time to @time', ['@time' => $current_time]);
      $this->state->set('audit_export_post.last_post_time', $current_time);

      if ($processed_count > 0) {
        $logger->info('Successfully processed @count audit reports during cron',
          ['@count' => $processed_count]);
      }
    }
    catch (\Exception $e) {
      $logger->error('Error in cron job: @message', ['@message' => $e->getMessage()]);
      // Log the stack trace for better debugging.
      $logger->error('Stack trace: @trace', ['@trace' => $e->getTraceAsString()]);
    }
  }

  /**
   * Process reports that were recently updated via queue processing.
   *
   * @param \Drupal\audit_export_core\Event\AuditExportQueueCompleteEvent $event
   *   The queue complete event.
   */
  public function onQueueComplete(AuditExportQueueCompleteEvent $event) {
    // Skip if remote posting is not enabled.
    if (!$this->configFactory->get('audit_export_post.settings')->get('enable_remote_post')) {
      return;
    }

    $audit_name = $event->getAuditName();

    try {
      // Get the report data and post it.
      $report_data = $this->auditReport->getReportData($audit_name);
      $result = $this->remotePost->postData($audit_name, $report_data);

      if ($result) {
        $this->loggerFactory->get('audit_export_post')->info('Posted queue-processed data for audit @audit.',
          ['@audit' => $audit_name]);
      }
      else {
        $this->loggerFactory->get('audit_export_post')->warning('Failed to post queue-processed data for audit @audit.',
          ['@audit' => $audit_name]);
      }
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('audit_export_post')->error('Error posting queue-processed data for @audit: @message',
        ['@audit' => $audit_name, '@message' => $e->getMessage()]);
    }
  }

}
