<?php

namespace Drupal\audit_export_post\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\audit_export_post\Service\AuditExportRemotePost;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for Audit Export Post operations.
 */
class AuditExportPostController extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The remote post service.
   *
   * @var \Drupal\audit_export_post\Service\AuditExportRemotePost
   */
  protected $remotePost;

  /**
   * Constructs a new AuditExportPostController.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\audit_export_post\Service\AuditExportRemotePost $remote_post
   *   The remote post service.
   */
  public function __construct(Connection $database, AuditExportRemotePost $remote_post) {
    $this->database = $database;
    $this->remotePost = $remote_post;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('audit_export_post.remote_post')
    );
  }

  /**
   * Posts an audit to the remote endpoint.
   *
   * @param string $audit_name
   *   The name of the audit to post.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response.
   */
  public function postAudit($audit_name) {
    // Check if remote posting is enabled.
    if (!$this->config('audit_export_post.settings')->get('enable_remote_post')) {
      $this->messenger()->addError($this->t('Remote posting is not enabled. Please enable it in the settings.'));
      return $this->redirect('audit_export_core.audit_export_reports');
    }

    // Get the report data from the database.
    $record = $this->database->select('audit_export_report', 'aer')
      ->fields('aer', ['data'])
      ->condition('audit', $audit_name)
      ->execute()
      ->fetchAssoc();

    if (empty($record) || empty($record['data'])) {
      $this->messenger()->addError($this->t('No data found for audit %audit.', ['%audit' => $audit_name]));
      return $this->redirect('audit_export_core.audit_export_reports');
    }

    // First try to decode as JSON (newer format)
    $data = json_decode($record['data'], TRUE);

    // If it's not valid JSON, safely unserialize the data (legacy format).
    if ($data === NULL && is_string($record['data'])) {
      // Unserialize with allowed_classes option set to false for security.
      $data = unserialize($record['data'], ['allowed_classes' => FALSE]);
    }

    // If we still don't have valid data, show an error.
    if (!is_array($data)) {
      $this->messenger()->addError($this->t('Could not parse data for audit %audit.', ['%audit' => $audit_name]));
      return $this->redirect('audit_export_core.audit_export_reports');
    }

    // Post the data to the remote endpoint.
    $result = $this->remotePost->postData($audit_name, $data);

    if ($result) {
      $this->messenger()->addStatus($this->t('Audit data successfully posted to the remote endpoint.'));
    }
    else {
      $this->messenger()->addError($this->t('Failed to post audit data to the remote endpoint. Check the logs for details.'));
    }

    // Redirect back to the reports page.
    return $this->redirect('audit_export_core.audit_export_reports');
  }

}
