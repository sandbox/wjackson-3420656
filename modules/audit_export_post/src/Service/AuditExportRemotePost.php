<?php

namespace Drupal\audit_export_post\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\audit_export_core\Service\AuditExportAuditReport;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Session\AccountProxyInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Service for handling remote post operations.
 */
class AuditExportRemotePost {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The audit report service.
   *
   * @var \Drupal\audit_export_core\Service\AuditExportAuditReport
   */
  protected $auditReport;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The audit export plugin manager.
   *
   * @var \Drupal\Core\Plugin\DefaultPluginManager
   */
  protected $pluginManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new AuditExportRemotePost object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\audit_export_core\Service\AuditExportAuditReport $audit_report
   *   The audit report service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Plugin\DefaultPluginManager $plugin_manager
   *   The plugin manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ClientInterface $http_client,
    LoggerChannelInterface $logger,
    ModuleHandlerInterface $module_handler,
    DateFormatterInterface $date_formatter,
    RequestStack $request_stack,
    AuditExportAuditReport $audit_report,
    EntityTypeManagerInterface $entity_type_manager,
    DefaultPluginManager $plugin_manager,
    AccountProxyInterface $current_user,
  ) {
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->logger = $logger;
    $this->moduleHandler = $module_handler;
    $this->dateFormatter = $date_formatter;
    $this->requestStack = $request_stack;
    $this->auditReport = $audit_report;
    $this->entityTypeManager = $entity_type_manager;
    $this->pluginManager = $plugin_manager;
    $this->currentUser = $current_user;
  }

  /**
   * Checks if remote post is enabled.
   *
   * @return bool
   *   TRUE if remote post is enabled, FALSE otherwise.
   */
  public function isEnabled() {
    return (bool) $this->configFactory->get('audit_export_post.settings')->get('enable_remote_post');
  }

  /**
   * Gets the remote post URL.
   *
   * @return string
   *   The remote post URL.
   */
  public function getRemotePostUrl() {
    $url = $this->configFactory->get('audit_export_post.settings')->get('remote_url');

    // Allow other modules to alter the URL.
    $this->moduleHandler->alter('audit_export_post_url', $url);

    return $url;
  }

  /**
   * Gets the site name.
   *
   * @return string
   *   The site name.
   */
  public function getSiteName() {
    $site_name = $this->configFactory->get('audit_export_post.settings')->get('site_name');

    if (empty($site_name)) {
      $site_name = $this->configFactory->get('system.site')->get('name');
    }

    return $site_name;
  }

  /**
   * Posts data to the remote endpoint.
   *
   * @param string $audit_name
   *   The audit name.
   * @param array $data
   *   The audit data.
   *
   * @return bool
   *   TRUE if the post was successful, FALSE otherwise.
   */
  public function postData($audit_name, array $data) {
    // Add detailed logging.
    $this->logger->debug('postData method called for audit: @audit', ['@audit' => $audit_name]);

    if (!$this->isEnabled()) {
      $this->logger->warning('Remote posting is disabled.');
      return FALSE;
    }

    $url = $this->getRemotePostUrl();

    if (empty($url)) {
      $this->logger->error('Remote URL is not configured.');
      return FALSE;
    }

    $this->logger->debug('Remote URL: @url', ['@url' => $url]);

    // Ensure the data is properly structured.
    $post_data = $this->buildPostData($audit_name, $data);

    // Log the data being sent for debugging.
    $data_excerpt = substr(json_encode($post_data, JSON_PRETTY_PRINT), 0, 500) . '...';
    $this->logger->debug('Sending data to remote endpoint for @audit: @data', [
      '@audit' => $audit_name,
      '@data' => $data_excerpt,
    ]);

    $config = $this->configFactory->get('audit_export_post.settings');

    // Prepare request options.
    $options = [
      // Use 'json' for automatic JSON encoding.
      'json' => $post_data,
      'timeout' => (int) $config->get('timeout') ?: 300,
      'connect_timeout' => 60,
      'verify' => (bool) $config->get('verify_ssl') ?? TRUE,
      'headers' => [
        'User-Agent' => 'Drupal Audit Export Post',
        'Content-Type' => 'application/json',
      ],
    ];

    // Log the timeout settings for debugging.
    $this->logger->debug('Request timeout: @timeout, connect timeout: @connect_timeout', [
      '@timeout' => $options['timeout'],
      '@connect_timeout' => $options['connect_timeout'],
    ]);

    // Add authentication if configured.
    $auth_type = $config->get('authentication_type');

    if ($auth_type === 'basic') {
      $username = $config->get('username');
      $password = $config->get('password');
      if (!empty($username) && !empty($password)) {
        $options['auth'] = [$username, $password];
        $this->logger->debug('Using Basic Authentication');
      }
      else {
        $this->logger->warning('Basic Authentication selected but username or password is empty');
      }
    }
    elseif ($auth_type === 'bearer') {
      $token = $config->get('token');
      if (!empty($token)) {
        $options['headers']['Authorization'] = 'Bearer ' . $token;
        $this->logger->debug('Using Bearer Token Authentication');
      }
      else {
        $this->logger->warning('Bearer Token Authentication selected but token is empty');
      }
    }
    else {
      $this->logger->debug('No authentication being used');
    }

    // Allow other modules to alter the request options.
    $this->moduleHandler->alter('audit_export_post_request_options', $options, $audit_name);

    try {
      $this->logger->debug('Sending HTTP request to @url', ['@url' => $url]);
      $response = $this->httpClient->request('POST', $url, $options);
      $status_code = $response->getStatusCode();
      $response_body = $response->getBody()->getContents();

      $this->logger->debug('Response status code: @code', ['@code' => $status_code]);
      $this->logger->debug('Response body: @body', ['@body' => substr($response_body, 0, 500) . '...']);

      if ($status_code >= 200 && $status_code < 300) {
        $this->logger->info('Successfully posted @audit data to @url. Status code: @code', [
          '@audit' => $audit_name,
          '@url' => $url,
          '@code' => $status_code,
        ]);
        return TRUE;
      }
      else {
        $this->logger->error('Failed to post @audit data to @url. Status code: @code', [
          '@audit' => $audit_name,
          '@url' => $url,
          '@code' => $status_code,
        ]);
        return FALSE;
      }
    }
    catch (GuzzleException $e) {
      $this->logger->error('Guzzle error posting @audit data to @url: @message', [
        '@audit' => $audit_name,
        '@url' => $url,
        '@message' => $e->getMessage(),
      ]);
      // Log additional details about the request.
      $this->logger->error('Request details: @details', [
        '@details' => json_encode($options),
      ]);
      return FALSE;
    }
    catch (\Exception $e) {
      $this->logger->error('Unexpected error posting @audit data to @url: @message', [
        '@audit' => $audit_name,
        '@url' => $url,
        '@message' => $e->getMessage(),
      ]);
      return FALSE;
    }
  }

  /**
   * Posts multiple audit reports in batch.
   *
   * @param array $audit_results
   *   An array of audit results from batch processing.
   *
   * @return array
   *   Array of audit names with success indicators.
   */
  public function postBatchData(array $audit_results) {
    if (!$this->isEnabled()) {
      return [];
    }

    $results = [];

    foreach ($audit_results as $audit_name => $audit_info) {
      if (!empty($audit_info['success'])) {
        try {
          // Get the full report data.
          $report_data = $this->auditReport->getReportData($audit_name);
          $results[$audit_name] = $this->postData($audit_name, $report_data);

          if ($results[$audit_name]) {
            $this->logger->info('Successfully posted batch data for audit @audit with @count records.', [
              '@audit' => $audit_name,
              '@count' => count($report_data),
            ]);
          }
        }
        catch (\Exception $e) {
          $results[$audit_name] = FALSE;
          $this->logger->error('Error posting batch data for @audit: @message', [
            '@audit' => $audit_name,
            '@message' => $e->getMessage(),
          ]);
        }
      }
      else {
        $results[$audit_name] = FALSE;
        $this->logger->warning('Skipping remote post for @audit due to processing errors.', [
          '@audit' => $audit_name,
        ]);
      }
    }

    return $results;
  }

  /**
   * Builds the post data.
   *
   * @param string $audit_name
   *   The audit name.
   * @param array $data
   *   The audit data.
   *
   * @return array
   *   The post data.
   */
  public function buildPostData($audit_name, array $data) {
    $this->logger->debug('Building post data for audit: @audit', ['@audit' => $audit_name]);

    // Get the Drupal version (using constant for simplicity)
    $drupal_version = \Drupal::VERSION;
    $server = $this->requestStack->getCurrentRequest()->server;
    $server_data = [];

    // Log server info for debugging.
    if ($server && method_exists($server, 'all')) {
      $server_data = $server->all();
      $this->logger->debug('Server data available: @keys', ['@keys' => implode(', ', array_keys($server_data))]);
    }
    else {
      $this->logger->warning('Server data not available in expected format');
    }

    // Prepare site name for use in identifiers.
    $site_name = $this->getSiteName();
    $this->logger->debug('Site name: @name', ['@name' => $site_name]);

    $site_name_clean = str_replace(' ', '_', $site_name);
    $site_name_clean = preg_replace('/[^a-zA-Z0-9_]/', '', $site_name_clean);

    // Get host and IP information safely.
    $host = $server_data['HTTP_HOST'] ?? 'unknown';
    $server_ip = $server_data['SERVER_ADDR'] ??
      ($server_data['REMOTE_ADDR'] ?? 'unknown');

    // Build site info.
    $site_info = [
      'site_label' => $site_name,
      'site_name' => strtolower($site_name_clean),
      'cms' => 'Drupal',
      'version' => $drupal_version,
      'version_major' => explode('.', $drupal_version)[0],
      'php_version' => phpversion(),
      'host' => $host,
      'server_ip_addr' => $server_ip,
      'date_generated' => $this->dateFormatter->format(time(), 'custom', 'Y-m-d H:i:s'),
      'site_id' => $data['site_info']['site_id'] ?? '',
    ];

    $this->logger->debug('Site info prepared: @info', ['@info' => json_encode($site_info)]);

    // Allow other modules to alter the site info.
    $this->moduleHandler->alter('audit_export_post_site_info', $site_info);

    // Get the headers from the plugin.
    $headers = [];
    try {
      if ($this->pluginManager) {
        $this->logger->debug('Using plugin manager to create instance of @audit', ['@audit' => $audit_name]);
        $plugin = $this->pluginManager->createInstance($audit_name);
        if ($plugin) {
          $headers = $plugin->getHeaders();
          $this->logger->debug('Retrieved headers from plugin: @headers', ['@headers' => json_encode($headers)]);
        }
        else {
          $this->logger->warning('Failed to create plugin instance for @audit', ['@audit' => $audit_name]);
        }
      }
      else {
        $this->logger->warning('Plugin manager is not available');
      }
    }
    catch (\Exception $e) {
      $this->logger->error('Error retrieving headers from plugin @audit: @message', [
        '@audit' => $audit_name,
        '@message' => $e->getMessage(),
      ]);
    }

    // Create a single combined data array with headers as the first row.
    $combined_data = [];

    // First add the headers.
    if (!empty($headers)) {
      $combined_data[] = $headers;
      $this->logger->debug('Added headers to combined data');
    }

    $row_count = 0;
    if (is_array($data)) {
      if (isset($data['data']) && is_array($data['data'])) {
        // If data is in a nested 'data' key.
        $this->logger->debug('Data is in nested "data" key with @count rows', ['@count' => count($data['data'])]);
        foreach ($data['data'] as $row) {
          $combined_data[] = $row;
          $row_count++;
        }
      }
      else {
        // If data is directly in the array.
        $this->logger->debug('Data is directly in array with @count items', ['@count' => count($data)]);
        foreach ($data as $row) {
          if (is_array($row)) {
            $combined_data[] = $row;
            $row_count++;
          }
        }
      }
    }

    $this->logger->debug('Added @count rows to combined data', ['@count' => $row_count]);

    // Build the complete report data structure.
    $report_data = [
      'audit' => $audit_name,
      'author' => $this->currentUser->id(),
      'date' => (string) time(),
      'data' => $combined_data,
    ];

    // Build the post data.
    $post_data = [
      'site_info' => $site_info,
      'report_data' => $report_data,
    ];

    $this->logger->debug('Built post data structure with @rows data rows',
      ['@rows' => count($combined_data)]);

    // Allow other modules to alter the post data.
    $this->moduleHandler->alter('audit_export_post_data', $post_data, $audit_name);

    return $post_data;
  }

}
