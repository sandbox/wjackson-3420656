<?php

namespace Drupal\audit_export\Plugin\AuditExport;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\audit_export_core\AuditExportPluginBase;

/**
 * Plugin implementation for auditing entity fields.
 *
 * @AuditExport(
 *   id = "entities",
 *   label = @Translation("Entity Field Audit"),
 *   description = @Translation("Audit field information for all entity types and bundles."),
 *   group = "entities",
 *   identifier = "entity_type",
 *   data_type = "flat",
 *   dependencies = {},
 * )
 */
final class AuditExportEntityField extends AuditExportPluginBase {

  /**
   * Constructor to set up the audit headers.
   */
  public function __construct() {
    // Use the same headers as the Drupal 7 version.
    $this->setHeaders(
      [
        'Entity',
        'Bundle',
        'Field',
        'Field Type',
        'Module',
        'Referenced Entity',
        'Referenced Bundle(s)',
        'Cardinality',
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function prepareData(): array {
    $entity_type_manager = \Drupal::entityTypeManager();
    $entity_field_manager = \Drupal::service('entity_field.manager');
    $bundle_info_service = \Drupal::service('entity_type.bundle.info');
    $entities = [];

    // Get all entity type definitions.
    $entity_types = $entity_type_manager->getDefinitions();

    // Iterate through each entity type definition.
    foreach ($entity_types as $entity_type_id => $entity_type) {
      // Check if entity type is content (similar to D7's entity_get_info()).
      if (!$entity_type instanceof ContentEntityTypeInterface) {
        continue;
      }

      // Get bundle information using the correct service.
      $bundles = $bundle_info_service->getBundleInfo($entity_type_id);

      foreach ($bundles as $bundle_name => $bundle_info) {
        // First, add base fields (similar to D7's default properties)
        $base_fields = $this->getDefaultProperties($entity_type_id);
        foreach ($base_fields as $base_field) {
          $entities[] = [
            'entity_type' => $entity_type_id,
            'bundle' => $bundle_name,
            'field_name' => $base_field,
            'is_base_field' => TRUE,
          ];
        }

        // Next, add configured fields (similar to D7's field_info_instances)
        $fields = $entity_field_manager->getFieldDefinitions($entity_type_id, $bundle_name);
        foreach ($fields as $field_name => $field_definition) {
          // Skip base fields that we've already added.
          if (in_array($field_name, $base_fields)) {
            continue;
          }

          $entities[] = [
            'entity_type' => $entity_type_id,
            'bundle' => $bundle_name,
            'field_name' => $field_name,
            'is_base_field' => FALSE,
          ];
        }
      }
    }

    return $entities;
  }

  /**
   * Provides a list of default properties for entities.
   *
   * This is similar to the Drupal 7 version's getDefaultProperties method.
   *
   * @param string $entity_type
   *   The type of entity to get properties for.
   *
   * @return array
   *   An array of property names.
   */
  protected function getDefaultProperties($entity_type): array {
    switch ($entity_type) {
      case 'taxonomy_term':
        return ['name', 'description'];

      case 'node':
        return ['title'];

      case 'user':
        return ['name', 'mail'];

      case 'comment':
        return ['subject', 'comment_body'];

      case 'media':
        return ['name'];

      default:
        // For other entity types, try to identify common base fields.
        $entity_type_manager = \Drupal::entityTypeManager();
        $entity_definition = $entity_type_manager->getDefinition($entity_type);
        $base_fields = [];

        // Add label field if available.
        $label_key = $entity_definition->getKey('label');
        if ($label_key) {
          $base_fields[] = $label_key;
        }

        return $base_fields;
    }
  }

  /**
   * Processes data for the audit.
   *
   * @param array $params
   *   Parameters for processing the data.
   *
   * @return array
   *   The processed data.
   */
  public function processData(array $params): array {
    // Initialize an empty array matching Drupal 7 output.
    $empty_data = [
    // Entity.
      '',
    // Bundle.
      '',
    // Field.
      '',
    // Field Type.
      '',
    // Module.
      '',
    // Referenced Entity.
      '',
    // Referenced Bundle(s)
      '',
    // Cardinality.
      '',
    ];

    // Ensure the 'row_data' key exists in params.
    if (empty($params['row_data'])) {
      \Drupal::logger('audit_export')->error('Missing row data in processData parameters.');
      return $empty_data;
    }

    $row_data = $params['row_data'];

    // Get the entity, bundle, and field from row_data.
    $entity_type_id = $row_data['entity_type'] ?? NULL;
    $bundle = $row_data['bundle'] ?? NULL;
    $field_name = $row_data['field_name'] ?? NULL;
    $is_base_field = $row_data['is_base_field'] ?? FALSE;

    if (!$entity_type_id || !$bundle || !$field_name) {
      \Drupal::logger('audit_export')->error('Invalid data provided for processing: @data', ['@data' => json_encode($row_data)]);
      return $empty_data;
    }

    try {
      // Initialize variables with default values.
      $field_type = 'property';
      $module = 'core';
      $referenced_entity_type = '';
      $referenced_bundles = '';
      $cardinality = 'Single';

      // If it's a base field (similar to D7's default properties)
      if ($is_base_field) {
        // Keep the default values set above.
      }
      else {
        // Get field definition from the entity field manager.
        $entity_field_manager = \Drupal::service('entity_field.manager');
        $field_definitions = $entity_field_manager->getFieldDefinitions($entity_type_id, $bundle);
        $field_definition = $field_definitions[$field_name] ?? NULL;

        if ($field_definition === NULL) {
          \Drupal::logger('audit_export')->error(
            'Field definition not found for entity: @entity_type, bundle: @bundle, field: @field', [
              '@entity_type' => $entity_type_id,
              '@bundle' => $bundle,
              '@field' => $field_name,
            ]
          );
          return $empty_data;
        }

        // Extract field details (similar to D7's field_info_field)
        $field_type = $field_definition->getType();
        $module = $field_definition->getFieldStorageDefinition()->getProvider();

        // Extract cardinality (match D7 terminology)
        $cardinality_value = $field_definition->getFieldStorageDefinition()->getCardinality();
        if ($cardinality_value === 1) {
          $cardinality = 'Single';
        }
        elseif ($cardinality_value === -1) {
          $cardinality = 'Unlimited';
        }
        else {
          $cardinality = $cardinality_value;
        }

        // Get reference information (match D7 output)
        $reference_info = $this->getReferenceEntities($field_definition);
        $referenced_entity_type = $reference_info['entity_type'];
        $referenced_bundles = strtolower($reference_info['bundles']);
      }

      // Return data in the same format as Drupal 7 version.
      return [
      // Entity.
        $entity_type_id,
      // Bundle.
        $bundle,
      // Field.
        $field_name,
      // Field Type.
        $field_type,
      // Module.
        $module,
      // Referenced Entity.
        $referenced_entity_type,
      // Referenced Bundle(s)
        $referenced_bundles,
      // Cardinality.
        $cardinality,
      ];
    }
    catch (\Exception $e) {
      \Drupal::logger('audit_export')->error(
        'Error processing data for entity: @entity_type, bundle: @bundle, field: @field. Error: @message', [
          '@entity_type' => $entity_type_id,
          '@bundle' => $bundle,
          '@field' => $field_name,
          '@message' => $e->getMessage(),
        ]
      );
      return $empty_data;
    }
  }

  /**
   * Helper function to get referenced entity types for reference fields.
   *
   * This matches the output format of the Drupal 7 version.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition object.
   *
   * @return array
   *   An array containing 'entity_type' and 'bundles' if applicable.
   */
  private function getReferenceEntities($field_definition): array {
    $info = [
      'entity_type' => '',
      'bundles' => '',
    ];

    // Check field type to determine reference type.
    switch ($field_definition->getType()) {
      case 'entity_reference':
        $target_type = $field_definition->getSetting('target_type');
        if ($target_type) {
          $info['entity_type'] = $target_type;

          // Get target bundles if available (similar to D7's handler_settings)
          $handler_settings = $field_definition->getSetting('handler_settings');
          $target_bundles = $handler_settings['target_bundles'] ?? [];

          if (!empty($target_bundles)) {
            // Convert to same format as D7 version.
            $info['bundles'] = implode(', ', array_keys($target_bundles));
          }
          else {
            // If no specific bundles, get all bundles for the entity type.
            $bundle_info_service = \Drupal::service('entity_type.bundle.info');
            $all_bundles = $bundle_info_service->getBundleInfo($target_type);
            $info['bundles'] = implode(', ', array_keys($all_bundles));
          }
        }
        break;

      case 'file':
      case 'image':
        $info['entity_type'] = 'file';
        $info['bundles'] = 'file';
        break;

      case 'link':
        // No entity reference.
        break;

      // Add more cases for other field types if needed.
      default:
        // Check if this is a custom entity reference field type.
        if (strpos($field_definition->getType(), 'entity_reference') !== FALSE) {
          // Handle custom entity reference field types.
          $target_type = $field_definition->getSetting('target_type');
          if ($target_type) {
            $info['entity_type'] = $target_type;

            // Handle target bundles similarly to entity_reference.
            $handler_settings = $field_definition->getSetting('handler_settings');
            $target_bundles = $handler_settings['target_bundles'] ?? [];

            if (!empty($target_bundles)) {
              $info['bundles'] = implode(', ', array_keys($target_bundles));
            }
            else {
              $bundle_info_service = \Drupal::service('entity_type.bundle.info');
              $all_bundles = $bundle_info_service->getBundleInfo($target_type);
              $info['bundles'] = implode(', ', array_keys($all_bundles));
            }
          }
        }
        break;
    }

    return $info;
  }

}
