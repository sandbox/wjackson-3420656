<?php

namespace Drupal\audit_export\Plugin\AuditExport;

use Drupal\audit_export_core\AuditExportPluginBase;
use Drupal\node\Entity\NodeType;

/**
 * Plugin implementation for auditing content types.
 *
 * @AuditExport(
 *   id = "content_type_audit",
 *   label = @Translation("Content Type Audit"),
 *   description = @Translation("Audit information about content types, including node counts and enabled displays."),
 *   group = "content",
 *   identifier = "machine_name",
 *   data_type = "flat",
 *   dependencies = {},
 * )
 */
final class AuditExportContentType extends AuditExportPluginBase {

  public function __construct() {
    $this->setHeaders(
          [
            'Content Type Name',
            'Machine Name',
            'Published Nodes',
            'Unpublished Nodes',
            'Enabled Displays',
          ]
      );
  }

  /**
   * {@inheritdoc}
   */
  public function prepareData(): array {
    $content_types = NodeType::loadMultiple();
    return array_keys($content_types);
  }

  /**
   * {@inheritdoc}
   */
  public function processData(array $params): array {
    $machine_name = $params["row_data"];
    $type = NodeType::load($machine_name);

    $published_count = $this->getNodeCountByStatus($machine_name, 1);
    $unpublished_count = $this->getNodeCountByStatus($machine_name, 0);
    $enabled_displays = $this->getEnabledDisplays($machine_name);

    return [
      $type->label(),
      $machine_name,
      $published_count,
      $unpublished_count,
      !empty($enabled_displays) ? implode(', ', $enabled_displays) : \Drupal::translation()->translate('No custom displays'),
    ];
  }

  /**
   * Gets the count of nodes by status for a specific content type.
   *
   * @param string $type
   *   The content type machine name.
   * @param int $status
   *   The node status (1 = published, 0 = unpublished).
   *
   * @return int
   *   The count of nodes.
   */
  protected function getNodeCountByStatus($type, $status): int {
    return \Drupal::entityQuery('node')
      ->condition('type', $type)
      ->condition('status', $status)
      ->accessCheck(FALSE)
      ->count()
      ->execute();
  }

  /**
   * Retrieves enabled displays (view modes) for a content type.
   *
   * @param string $type
   *   The content type machine name.
   *
   * @return array
   *   An array of enabled display names.
   */
  protected function getEnabledDisplays($type): array {
    // Get the entity display repository service.
    $display_repository = \Drupal::service('entity_display.repository');

    // Get all available view modes for nodes.
    $view_modes = $display_repository->getViewModes('node');

    $enabled_displays = [];

    // Always include default display as it's always enabled.
    $enabled_displays['default'] = 'Default';

    // Get the entity view display storage.
    $storage = \Drupal::entityTypeManager()->getStorage('entity_view_display');

    // Check each view mode.
    foreach ($view_modes as $machine_name => $view_mode) {
      // Load the entity view display for this bundle and view mode.
      $view_display = $storage->load("node.{$type}.{$machine_name}");

      // Only include if the view display exists and is enabled.
      if ($view_display && $view_display->status()) {
        $enabled_displays[$machine_name] = $view_mode['label'];
      }
    }

    // Sort displays alphabetically by label.
    asort($enabled_displays);

    return $enabled_displays;
  }

}
