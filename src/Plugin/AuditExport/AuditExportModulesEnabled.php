<?php

namespace Drupal\audit_export\Plugin\AuditExport;

use Drupal\audit_export_core\AuditExportPluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Plugin implementation of the audit_export for enabled modules.
 *
 * @AuditExport(
 *   id = "enabled_modules",
 *   label = @Translation("Modules Enabled"),
 *   description = @Translation("Audit enabled modules, including version, status, and type."),
 *   data_type = "flat",
 *   identifier = "module_name",
 *   group = "modules",
 *   dependencies = {}
 * )
 */
final class AuditExportModulesEnabled extends AuditExportPluginBase {

  use StringTranslationTrait;

  /**
   * Stores update information for all projects.
   *
   * @var array
   */
  protected $updateInfo;

  /**
   * Indicates whether the Update Manager module is enabled.
   *
   * @var bool
   */
  protected $updateManagerEnabled;

  /**
   * Stores the current Drupal version.
   *
   * @var string
   */
  protected $drupalVersion;

  /**
   * Constructs the AuditExportModulesEnabled object.
   */
  public function __construct() {
    $this->setHeaders(
          [
            'Module Name (machine_name)',
            'Current Version',
            'Latest Release',
            'Status',
            'Type',
            'Parent Module',
          ]
      );

    // Check for Update Manager module availability.
    $this->updateManagerEnabled = \Drupal::moduleHandler()->moduleExists('update') && function_exists('update_get_available');
    $this->drupalVersion = \Drupal::VERSION;

    // Fetch update information if the Update Manager is enabled.
    if ($this->updateManagerEnabled) {
      $update_manager = \Drupal::service('update.manager');
      $this->updateInfo = $update_manager->getProjects();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prepareData(): array {
    $modules = \Drupal::service('extension.list.module')->getList();

    // Filter out install profiles and non-enabled modules.
    $enabled_modules = array_filter($modules, function ($module) {
      // Skip if it's not enabled.
      if (!$module->status) {
        return FALSE;
      }

      // Skip install profiles.
      $module_path = $module->getPath();
      if (strpos($module_path, '/core/profiles/') !== FALSE ||
        strpos($module_path, '/profiles/') === 0) {
        return FALSE;
      }

      // Also skip if the info file specifies this is a profile.
      if (isset($module->info['type']) && $module->info['type'] === 'profile') {
        return FALSE;
      }

      return TRUE;
    });

    return array_keys($enabled_modules);
  }

  /**
   * {@inheritdoc}
   */
  public function processData(array $params): array {
    $machine_name = $params['row_data'];
    $modules = \Drupal::service('extension.list.module')->getList();

    if (isset($modules[$machine_name])) {
      $module = $modules[$machine_name];
      [$module_type, $parent_module] = $this->determineModuleType($module->getName());
      $status = $module->status ? $this->t('Enabled') : $this->t('Disabled');
      $latest_release = $module_type === 'Core' ? $this->drupalVersion : $this->getLatestRelease($machine_name);

      return [
        $module->info['name'] . ' (' . $machine_name . ')',
        $module->info['version'],
        $latest_release,
        $status,
        $module_type,
        $parent_module,
      ];
    }

    return [];
  }

  /**
   * Determines module type based on info.yml, composer.json, relationships.
   *
   * @param string $module_name
   *   The machine name of the module.
   *
   * @return array
   *   An array containing the type of the module associated parent module.
   *   if it is a submodule.
   */
  private function determineModuleType($module_name) {
    $module_path = \Drupal::service('extension.list.module')->getPath($module_name);
    $info_file_path = $module_path . "/$module_name.info.yml";
    $parent_module = '';

    if (!file_exists($info_file_path)) {
      return ['Unknown', $parent_module];
    }

    // Parse the module's info file.
    $info = \Drupal::service('info_parser')->parse($info_file_path);

    // Method 1: Check if this is a core module based on the package value.
    if (isset($info['package']) && $info['package'] === 'Core') {
      return ['Core', $parent_module];
    }

    // Method 2: Check if the module is in the core directory.
    if (strpos($module_path, 'core/modules/') === 0) {
      return ['Core', $parent_module];
    }

    // Method 3: Experimental core modules might be in core/experimental.
    if (strpos($module_path, 'core/experimental/') === 0) {
      return ['Core (Experimental)', $parent_module];
    }

    // Check for submodules by examining parent directories.
    $parent_directory = dirname($module_path);
    $parent_info_file = $parent_directory . '/' . basename($parent_directory) . '.info.yml';

    if (file_exists($parent_info_file)) {
      $parent_module = basename($parent_directory);
      return ['Submodule', $parent_module];
    }

    // Check grandparent directory for deeper nested submodules.
    $grandparent_directory = dirname($parent_directory);
    $grandparent_info_file = $grandparent_directory . '/' . basename($grandparent_directory) . '.info.yml';

    if (file_exists($grandparent_info_file)) {
      $parent_module = basename($grandparent_directory);
      return ['Submodule', $parent_module];
    }

    // Method 4: Check for Drupal.org packaging script information in info.yml.
    if (isset($info['project']) && $info['project'] !== 'drupal') {
      // Project is set and it's not the 'drupal' core project.
      return ['Contributed', $parent_module];
    }

    // Method 5: Check for composer.json to identify contributed modules.
    $composer_file = $module_path . '/composer.json';
    if (file_exists($composer_file)) {
      $composer_data = json_decode(file_get_contents($composer_file), TRUE);

      // Check if the module follows the drupal/* naming convention.
      if (isset($composer_data['name']) &&
        strpos($composer_data['name'], 'drupal/') === 0) {
        return ['Contributed', $parent_module];
      }

      // If there's a composer.json but not following drupal namespace,
      // check for common contributed module indicators.
      if (isset($composer_data['type']) &&
        $composer_data['type'] === 'drupal-module' &&
        (!isset($composer_data['type']) || $composer_data['type'] !== 'drupal-custom-module')) {
        // Check for license field which is common in contributed modules.
        if (isset($composer_data['license'])) {
          return ['Contributed', $parent_module];
        }
      }
    }

    // If we get here, look for the raw info.yml to check for packaging script.
    if (file_exists($info_file_path)) {
      $info_content = file_get_contents($info_file_path);
      if (strpos($info_content, '# Information added by Drupal.org packaging script') !== FALSE) {
        return ['Contributed', $parent_module];
      }
    }

    // Default to Custom for any module not identified as Core or Contributed.
    return ['Custom', $parent_module];
  }

  /**
   * Fetches the latest recommended release version for a module.
   *
   * @param string $machine_name
   *   The machine name of the module.
   *
   * @return string
   *   The latest recommended release version or a status message.
   */
  private function getLatestRelease($machine_name) {
    // Only use the Update module API since it's the authoritative source.
    if (!$this->updateManagerEnabled) {
      return $this->t('Update Manager Disabled');
    }

    // Get the raw available updates data.
    $available = update_get_available(TRUE);

    // For Drupal core, we need special handling.
    if ($machine_name === 'core') {
      $machine_name = 'drupal';
    }

    if (empty($available) || !isset($available[$machine_name])) {
      return $this->t('N/A');
    }

    // Load the comparison logic.
    if (!function_exists('update_calculate_project_data')) {
      $this->moduleHandler()->loadInclude('update', 'compare.inc');
    }

    // Calculate the project data using core's method.
    $projects = update_calculate_project_data($available);

    if (!isset($projects[$machine_name])) {
      return $this->t('N/A');
    }

    $project = $projects[$machine_name];

    // Check if there's a security update available.
    $has_security_update = FALSE;

    if (isset($project['status']) && $project['status'] == 4) {
      $has_security_update = TRUE;
    }

    // Check for recommended release.
    if (!empty($project['recommended']) && isset($project['releases'][$project['recommended']])) {
      $version = $project['releases'][$project['recommended']]['version'];

      if ($has_security_update) {
        return $version . ' ' . $this->t('(Security update!)');
      }
      return $version;
    }

    // If no recommended release, check for latest version.
    if (!empty($project['latest_version']) && isset($project['releases'][$project['latest_version']])) {
      $version = $project['latest_version'];

      if ($has_security_update) {
        return $version . ' ' . $this->t('(Security update!)');
      }
      return $version;
    }

    // Check for installed version as fallback.
    if (!empty($project['existing_version'])) {
      return $project['existing_version'] . ' ' . $this->t('(current)');
    }

    // If we have status info but no usable version.
    return $this->t('No releases available');
  }

}
