<?php

namespace Drupal\audit_export\Plugin\AuditExport;

use Drupal\audit_export_core\AuditExportPluginBase;
use Drupal\Core\Menu\MenuTreeParameters;

/**
 * Plugin implementation of the audit_export for Menus.
 *
 * @AuditExport(
 *   id = "menus_audit",
 *   label = @Translation("Active Menus"),
 *   description = @Translation("Audit menu data and associated configurations within Drupal."),
 *   group = "content",
 *   identifier = "menu",
 *   data_type = "flat",
 *   dependencies = {},
 * )
 */
final class AuditExportMenus extends AuditExportPluginBase {

  /**
   * Constructor for AuditExportMenus.
   */
  public function __construct() {
    $this->setHeaders(
          [
            'Menu',
            'Menu Block',
            'Module',
            'Links',
            'Menu Usage',
            'Starting level',
            'Max depth',
            'Has expanded children',
          ]
      );
  }

  /**
   * {@inheritdoc}
   */
  public function prepareData(): array {
    // Retrieve all enabled menus.
    $menus = \Drupal::entityTypeManager()->getStorage('menu')->loadMultiple();
    $system_menus = $this->getSystemMenus();
    $menu_machine_names = array_keys($menus);

    // Get module-provided and database menus.
    $module_menus = $this->getModuleMenus($menu_machine_names);

    $return = [];
    foreach ($menus as $menu) {
      $menu_name = $menu->id();
      // Check system menus first, then module menus, fallback to database.
      $source = $system_menus[$menu_name] ?? $module_menus[$menu_name] ?? 'database';

      $return[] = [
        'menu' => $menu_name,
        'module' => $source,
      ];
    }

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function processData(array $params): array {
    $menu_name = $params["row_data"]["menu"];
    $menu = \Drupal::entityTypeManager()->getStorage('menu')->load($menu_name);

    // Retrieve menu links and blocks related to the menu.
    $menu_links = $this->getMenuLinks($menu_name);
    $blocks = $this->getMenuBlocks($menu_name);

    // Aggregate information.
    $menu_info = $this->aggregateMenuInfo($menu_links);
    $block_info = $this->aggregateBlockInfo($blocks, $menu);

    // Get menu description, fallback to 'No description' if not set.
    $menu_description = $menu->getDescription() ?: 'No description';

    return [
      $menu->label() . " ($menu_name)",
      $block_info["title"],
      $params["row_data"]["module"],
      count($menu_links),
      $menu_description,
      $menu_info['start_depth'] ?? 'N/A',
      $menu_info['max_depth'] ?? 'N/A',
      $menu_info['has_children'] ? 'Yes' : 'No',
    ];
  }

  /**
   * Retrieve system menus by scanning core system module config.
   *
   * @return array
   *   Array of system menu machine names mapped to 'system'.
   */
  private function getSystemMenus(): array {
    $system_menus = [];
    $system_config_path = DRUPAL_ROOT . '/core/modules/system/config/install';

    if (is_dir($system_config_path)) {
      $files = scandir($system_config_path);
      foreach ($files as $file) {
        // Look for system menu config files.
        if (preg_match('/^system\.menu\.(.+)\.yml$/', $file, $matches)) {
          $menu_name = $matches[1];
          $system_menus[$menu_name] = 'system';
        }
      }
    }

    return $system_menus;
  }

  /**
   * Determines the source module for menus.
   *
   * @param array $menus
   *   Array of menu machine names to check.
   *
   * @return array
   *   Array of menu names mapped to their providing modules or 'database'.
   */
  private function getModuleMenus(array $menus): array {
    $module_menus = [];
    $module_handler = \Drupal::moduleHandler();
    $module_list = $module_handler->getModuleList();
    $config_factory = \Drupal::configFactory();

    foreach ($menus as $menu_name) {
      $found = FALSE;

      // Check each module's config/install and config/optional directories.
      foreach ($module_list as $module_name => $extension) {
        // Skip the system module as it's handled by getSystemMenus()
        if ($module_name === 'system') {
          continue;
        }

        $module_path = $extension->getPath();
        $config_paths = [
          DRUPAL_ROOT . '/' . $module_path . '/config/install/system.menu.' . $menu_name . '.yml',
          DRUPAL_ROOT . '/' . $module_path . '/config/optional/system.menu.' . $menu_name . '.yml',
        ];

        foreach ($config_paths as $config_file) {
          if (file_exists($config_file)) {
            $module_menus[$menu_name] = $module_name;
            $found = TRUE;
            break 2;
          }
        }
      }

      if (!$found) {
        // Check active configuration.
        $config = $config_factory->get('system.menu.' . $menu_name);
        if (!$config->isNew()) {
          // Check if this config has module dependencies.
          $dependencies = $config->get('dependencies.module') ?? [];
          if (!empty($dependencies)) {
            $module_menus[$menu_name] = reset($dependencies);
          }
          else {
            $module_menus[$menu_name] = 'database';
          }
        }
        else {
          $module_menus[$menu_name] = 'database';
        }
      }
    }

    return $module_menus;
  }

  /**
   * Retrieves menu links for a specific menu.
   *
   * @param string $menu_name
   *   The menu machine name.
   *
   * @return array
   *   Array of menu links.
   */
  private function getMenuLinks(string $menu_name): array {
    // Get the menu tree service.
    $menu_tree = \Drupal::service('menu.link_tree');

    // Get the menu tree parameters.
    $parameters = new MenuTreeParameters();
    $parameters->onlyEnabledLinks();

    // Load the tree.
    $tree = $menu_tree->load($menu_name, $parameters);

    // Apply manipulators like access checking.
    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    $tree = $menu_tree->transform($tree, $manipulators);

    // Flatten the tree into a simple array of links.
    $links = [];
    $this->flattenMenuTree($tree, $links);

    return $links;
  }

  /**
   * Helper function to flatten a menu tree into an array.
   *
   * @param array $tree
   *   The menu link tree.
   * @param array &$links
   *   Array to store the flattened links.
   */
  private function flattenMenuTree(array $tree, array &$links): void {
    foreach ($tree as $item) {
      if (!$item->link) {
        continue;
      }

      $links[] = [
        'id' => $item->link->getPluginId(),
        'title' => $item->link->getTitle(),
        'url' => $item->link->getUrlObject(),
        'has_children' => !empty($item->subtree),
        'depth' => $item->depth,
      ];

      if (!empty($item->subtree)) {
        $this->flattenMenuTree($item->subtree, $links);
      }
    }
  }

  /**
   * Retrieves blocks that reference a specific menu.
   */
  private function getMenuBlocks(string $menu_name): array {
    $block_manager = \Drupal::service('plugin.manager.block');
    $blocks = [];

    foreach ($block_manager->getDefinitions() as $block_definition) {
      if (isset($block_definition['menu_name']) && $block_definition['menu_name'] === $menu_name) {
        $blocks[] = $block_definition;
      }
    }

    return $blocks;
  }

  /**
   * Aggregates information from menu links.
   */
  private function aggregateMenuInfo(array $menu_links): array {
    if (empty($menu_links)) {
      return [
        'start_depth' => 'N/A',
        'max_depth' => 'N/A',
        'has_children' => FALSE,
      ];
    }

    $depths = array_column($menu_links, 'depth');
    return [
      'start_depth' => min($depths),
      'max_depth' => max($depths),
      'has_children' => in_array(1, array_column($menu_links, 'has_children')),
    ];
  }

  /**
   * Aggregates information from blocks.
   */
  private function aggregateBlockInfo(array $blocks, $menu): array {
    $custom_titles = [];
    foreach ($blocks as $block) {
      if ($block['status'] && !empty($block['title'])) {
        $custom_titles[$block['theme']] = $block['title'];
      }
    }

    return [
      'title' => $custom_titles ?: $menu->label(),
    ];
  }

}
