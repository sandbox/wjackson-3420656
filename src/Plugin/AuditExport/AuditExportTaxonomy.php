<?php

namespace Drupal\audit_export\Plugin\AuditExport;

use Drupal\audit_export_core\AuditExportPluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\taxonomy\Entity\Vocabulary;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Plugin implementation of the audit_export for taxonomy vocabularies.
 *
 * @AuditExport(
 *   id = "taxonomy_vocabulary",
 *   label = @Translation("Taxonomy Vocabularies"),
 *   description = @Translation("Audit taxonomy vocabularies and their usage."),
 *   data_type = "flat",
 *   identifier = "id",
 *   group = "taxonomy",
 *   dependencies = {}
 * )
 */
final class AuditExportTaxonomy extends AuditExportPluginBase {
  use StringTranslationTrait;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->database = $container->get('database');
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->entityTypeBundleInfo = $container->get('entity_type.bundle.info');
    $instance->loggerFactory = $container->get('logger.factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ?EntityTypeManagerInterface $entity_type_manager = NULL,
    ?Connection $database = NULL,
    ?EntityFieldManagerInterface $entity_field_manager = NULL,
    ?EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL,
    ?LoggerChannelFactoryInterface $logger_factory = NULL,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager);

    // Store additional services.
    $this->database = $database;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->loggerFactory = $logger_factory;

    // Define headers for the report.
    $this->setHeaders(
      [
        $this->t('Vocabulary Name (machine_name)'),
        $this->t('Count of Terms'),
        $this->t('Max Depth'),
        $this->t('Count of Fields Referencing Vocabulary'),
        $this->t('Has Custom Fields'),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function prepareData(): array {
    // Fetch all vocabulary IDs using injected entity type manager.
    if ($this->entityTypeManager) {
      return $this->entityTypeManager->getStorage('taxonomy_vocabulary')
        ->getQuery()
        ->execute();
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function processData(array $params): array {
    $vid = $params['row_data'];

    // Skip processing if essential services are not available.
    if (!$this->entityTypeManager) {
      return [];
    }

    // Load the vocabulary by ID.
    $vocab = Vocabulary::load($vid);

    if ($vocab) {
      $vocab_name = $vocab->label();
      $vocab_machine_name = $vocab->id();

      // Count the terms in the vocabulary.
      $term_count = $this->entityTypeManager->getStorage('taxonomy_term')
        ->getQuery()
        ->condition('vid', $vocab_machine_name)
        ->accessCheck(FALSE)
        ->count()
        ->execute();

      // Get the maximum depth of terms.
      $max_depth = $this->calculateMaxDepth($vocab_machine_name);

      // Calculate field references and custom fields.
      $field_count = $this->calculateFieldReferences($vocab_machine_name);
      $has_custom_fields = $this->checkForCustomFields($vocab_machine_name);

      // Return processed data.
      return [
        $vocab_name . ' (' . $vocab_machine_name . ')',
        $term_count,
        $max_depth,
        $field_count,
        $has_custom_fields ? $this->t('Yes') : $this->t('No'),
      ];
    }

    return [];
  }

  /**
   * Calculates the maximum depth of terms in a vocabulary.
   *
   * @param string $vocab_machine_name
   *   The machine name of the vocabulary.
   *
   * @return int
   *   The maximum depth of terms in the vocabulary. 0 if no terms exist.
   */
  private function calculateMaxDepth(string $vocab_machine_name): int {
    $max_depth = 0;

    // Skip if database service is not available.
    if (!$this->database) {
      return $max_depth;
    }

    try {
      // Query to get all terms with their parents.
      $query = $this->database->select('taxonomy_term_field_data', 't');
      $query->join('taxonomy_term__parent', 'p', 't.tid = p.entity_id');
      $query->fields('t', ['tid'])
        ->fields('p', ['parent_target_id'])
        ->condition('t.vid', $vocab_machine_name);
      $terms = $query->execute()->fetchAll();

      // Build a parent-child relationship array.
      $relationships = [];
      foreach ($terms as $term) {
        $relationships[$term->tid] = $term->parent_target_id;
      }

      // Calculate depth for each term.
      foreach ($relationships as $parent_id) {
        $depth = 1;
        $current_parent = $parent_id;

        // Follow the chain of parents until we reach the root (0)
        while ($current_parent != 0 && isset($relationships[$current_parent])) {
          $depth++;
          $current_parent = $relationships[$current_parent];

          // Prevent infinite loops in case of circular references.
          if ($depth > 100) {
            if ($this->loggerFactory) {
              $this->loggerFactory->get('audit_export')->warning(
                'Possible circular reference detected in taxonomy terms for vocabulary @vid',
                ['@vid' => $vocab_machine_name]
              );
            }
            break;
          }
        }

        $max_depth = max($max_depth, $depth);
      }

      // Also check for terms without parents (depth = 1)
      if ($max_depth === 0) {
        $root_terms = $this->database->select('taxonomy_term_field_data', 't')
          ->condition('t.vid', $vocab_machine_name)
          ->countQuery()
          ->execute()
          ->fetchField();

        if ($root_terms > 0) {
          $max_depth = 1;
        }
      }
    }
    catch (\Exception $e) {
      if ($this->loggerFactory) {
        $this->loggerFactory->get('audit_export')->error(
          'Error calculating taxonomy depth for vocabulary @vid: @message',
          [
            '@vid' => $vocab_machine_name,
            '@message' => $e->getMessage(),
          ]
        );
      }
    }

    return $max_depth;
  }

  /**
   * Checks if a vocabulary has any custom fields.
   *
   * @param string $vid
   *   The vocabulary machine name.
   *
   * @return bool
   *   TRUE if the vocabulary has custom fields, FALSE otherwise.
   */
  private function checkForCustomFields(string $vid): bool {
    // Skip if entity field manager is not available.
    if (!$this->entityFieldManager) {
      return FALSE;
    }

    // Get all field definitions for taxonomy terms of this vocabulary.
    $fieldDefinitions = $this->entityFieldManager->getFieldDefinitions('taxonomy_term', $vid);

    // Get base field definitions to exclude them.
    $baseFieldDefinitions = $this->entityFieldManager->getBaseFieldDefinitions('taxonomy_term');

    // Filter out base fields to only get custom fields.
    $customFields = array_diff_key($fieldDefinitions, $baseFieldDefinitions);

    // If there are any custom fields, return true.
    return !empty($customFields);
  }

  /**
   * Calculates the number of fields referencing a vocabulary.
   *
   * @param string $machine_name
   *   The machine name of the vocabulary.
   *
   * @return int
   *   The number of fields referencing this vocabulary.
   */
  private function calculateFieldReferences(string $machine_name): int {
    $field_count = 0;

    // Skip if essential services are not available.
    if (!$this->entityTypeManager || !$this->entityFieldManager || !$this->entityTypeBundleInfo) {
      return $field_count;
    }

    // Get all entity type definitions.
    $entity_types = $this->entityTypeManager->getDefinitions();

    // Iterate through each entity type.
    foreach ($entity_types as $entity_type_id => $entity_type) {
      // Skip if not a content entity.
      if (!$entity_type->entityClassImplements('\Drupal\Core\Entity\ContentEntityInterface')) {
        continue;
      }

      // Get bundles for this entity type.
      $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);

      // Check each bundle.
      foreach ($bundles as $bundle_id => $bundle_info) {
        // Get all field definitions for this bundle.
        $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle_id);

        // Check each field.
        foreach ($field_definitions as $field_definition) {
          // Check entity reference field targeting taxonomy terms.
          if ($field_definition->getType() === 'entity_reference' &&
            $field_definition->getSetting('target_type') === 'taxonomy_term') {

            // Get the handler settings.
            $handler_settings = $field_definition->getSetting('handler_settings');

            // Check if this field references our vocabulary.
            if (isset($handler_settings['target_bundles']) &&
              (in_array($machine_name, $handler_settings['target_bundles']) ||
                array_key_exists($machine_name, $handler_settings['target_bundles']))) {

              // Increment field count.
              $field_count++;
            }
          }
        }
      }
    }

    return $field_count;
  }

}
