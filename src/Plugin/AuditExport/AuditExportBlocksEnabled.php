<?php

namespace Drupal\audit_export\Plugin\AuditExport;

use Drupal\audit_export_core\AuditExportPluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ModuleExtensionList;

/**
 * Plugin implementation of the audit_export.
 *
 * @AuditExport(
 *   id = "blocks_enabled",
 *   label = @Translation("Blocks Enabled"),
 *   description = @Translation("Report on all blocks that are enabled and currently in use."),
 *   group = "content",
 *   identifier = "block",
 *   data_type = "flat",
 *   dependencies = {},
 * )
 */
final class AuditExportBlocksEnabled extends AuditExportPluginBase {

  use StringTranslationTrait;

  /**
   * The context module status.
   *
   * @var bool
   */
  protected $contextModuleEnabled;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->moduleHandler = $container->get('module_handler');
    $instance->themeHandler = $container->get('theme_handler');
    $instance->moduleExtensionList = $container->get('extension.list.module');
    $instance->contextModuleEnabled = $instance->moduleHandler->moduleExists('context');
    return $instance;
  }

  /**
   * Build headers for Blocks Enabled report.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface|null $module_handler
   *   The module handler.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface|null $theme_handler
   *   The theme handler.
   * @param \Drupal\Core\Extension\ModuleExtensionList|null $module_extension_list
   *   The module extension list.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ?EntityTypeManagerInterface $entity_type_manager = NULL,
    ?ModuleHandlerInterface $module_handler = NULL,
    ?ThemeHandlerInterface $theme_handler = NULL,
    ?ModuleExtensionList $module_extension_list = NULL,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager);
    $this->moduleHandler = $module_handler;
    $this->themeHandler = $theme_handler;
    $this->moduleExtensionList = $module_extension_list;
    $this->contextModuleEnabled = $module_handler ? $module_handler->moduleExists('context') : FALSE;
    $this->setHeaders(['Block', 'Module', 'Theme', 'Placement', 'Visibility']);
  }

  /**
   * Return all block ids for processing.
   *
   * @return array
   *   An array of blocks with their types.
   */
  public function prepareData(): array {
    $blocks = [];

    // Add common blocks.
    foreach ($this->fetchEnabledBlockIds() as $block_id) {
      $blocks[] = [
        'type' => 'common',
        'block' => $block_id,
      ];
    }

    // Add context blocks if the module is enabled.
    if ($this->contextModuleEnabled) {
      foreach ($this->fetchContextBlocks() as $context_block) {
        $blocks[] = [
          'type' => 'context',
          'block' => $context_block,
        ];
      }
    }

    return $blocks;
  }

  /**
   * Process all block ids.
   *
   * @param array $params
   *   The parameters containing the row data to process.
   *
   * @return array
   *   An array of processed block data.
   */
  public function processData(array $params = []): array {
    $block = $params["row_data"];

    if ($block["type"] === 'common') {
      $block_info = $this->fetchBlockInfo($block["block"]);
      return $this->getCommonBlockData($block_info);
    }

    if ($this->contextModuleEnabled && $block["type"] === 'context') {
      return $this->getContextBlockData($block["block"]);
    }

    return [];
  }

  /**
   * Fetch block data for common blocks.
   *
   * @param array $block_info
   *   The block information.
   *
   * @return array
   *   Processed block data.
   */
  private function getCommonBlockData(array $block_info): array {
    if ($this->isReferenceThemeEnabled($block_info)) {
      $block_title = $this->getBlockTitle($block_info);
      $module_info = $this->getModuleInfo($block_info);
      $theme = $block_info['theme'];
      $region = $this->getRegionName($block_info['region']);

      // Check if 'pages' is set to a string; otherwise provide default value.
      $pages = isset($block_info['pages']) && is_string($block_info['pages'])
        ? $this->cleanArrayChars($block_info['pages'])
        : $this->t('No pages specified');

      $visibility = $this->getBlockVisibility($block_info['visibility'] ?? 0, $pages);

      return [
        $block_title,
        $module_info,
        $theme,
        $region,
        $visibility,
      ];
    }
    return [];
  }

  /**
   * Get the module information for a block.
   *
   * @param array $block_info
   *   The block information.
   *
   * @return string
   *   The formatted module information.
   */
  private function getModuleInfo(array $block_info): string {
    $module = $block_info['provider'] ?? $block_info['module'] ?? 'system';

    // Fix: Check if module is "core" and handle differently.
    if ($module === 'core') {
      return 'Drupal Core';
    }

    if ($this->moduleHandler && $this->moduleExtensionList) {
      try {
        // Use extension.list.module service directly.
        if ($this->moduleHandler->moduleExists($module)) {
          $module_info = $this->moduleExtensionList->getExtensionInfo($module);
          $module_name = $module_info['name'] ?? $module;
          return sprintf('%s (%s)', $module_name, $module);
        }
      }
      catch (\Exception $e) {
        // If we can't get extension info, just return module machine name.
        return $module;
      }
    }

    return $module;
  }

  /**
   * Fetch enabled block IDs.
   *
   * @return array
   *   An array of enabled block IDs.
   */
  private function fetchEnabledBlockIds(): array {
    if ($this->entityTypeManager) {
      return array_values(
        $this->entityTypeManager->getStorage('block')
          ->getQuery()
          ->condition('status', 1)
          ->execute()
      );
    }
    return [];
  }

  /**
   * Fetch context blocks if the context module is enabled.
   *
   * @return array
   *   An array of context blocks.
   */
  private function fetchContextBlocks(): array {
    $context_blocks = [];
    if (!$this->contextModuleEnabled) {
      return [];
    }

    return $context_blocks;
  }

  /**
   * Fetch block info from the block entity.
   *
   * @param string $bid
   *   The block ID.
   *
   * @return array
   *   Block information.
   */
  private function fetchBlockInfo($bid): array {
    if (!$this->entityTypeManager) {
      return [];
    }

    $block = $this->entityTypeManager->getStorage('block')->load($bid);

    if ($block) {
      $plugin = $block->getPlugin();
      $plugin_definition = $plugin->getPluginDefinition();

      return [
        'id' => $block->id(),
        'label' => $block->label(),
        'region' => $block->getRegion(),
        'theme' => $block->getTheme(),
        'status' => $block->status(),
        'provider' => $plugin_definition['provider'] ?? 'system',
        'module' => $plugin_definition['id'] ?? $plugin->getBaseId(),
      ];
    }

    return [];
  }

  /**
   * Get the title of a block.
   *
   * @param array $block_info
   *   The block information.
   *
   * @return string
   *   The block title.
   */
  private function getBlockTitle(array $block_info): string {
    $label = $block_info['label'] ?? $this->t('Untitled block');
    $id = $block_info['id'] ?? '';
    return !empty($id) ? sprintf('%s (%s)', $label, $id) : $label;
  }

  /**
   * Get block visibility as a string.
   *
   * @param int $visibility
   *   Block visibility setting.
   * @param string $pages
   *   Pages where the block is shown or hidden.
   *
   * @return string
   *   Block visibility description.
   */
  private function getBlockVisibility($visibility, $pages): string {
    return $visibility == 1 ?
      $this->t('Only on pages:') . ' ' . $pages :
      $this->t('All pages');
  }

  /**
   * Get the name of the region where the block is placed.
   *
   * @param string $region
   *   The block region machine name.
   *
   * @return string
   *   The block region name.
   */
  private function getRegionName($region): string {
    return $this->t('@region', ['@region' => $region]);
  }

  /**
   * Clean up special characters from the pages string.
   *
   * @param string $string
   *   The string to clean.
   *
   * @return string
   *   The cleaned string.
   */
  private function cleanArrayChars(string $string): string {
    return str_replace(["\r", "\n"], ', ', $string);
  }

  /**
   * Check if the block's theme is enabled.
   *
   * @param array $block_info
   *   The block information.
   *
   * @return bool
   *   TRUE if the theme is enabled, FALSE otherwise.
   */
  private function isReferenceThemeEnabled(array $block_info): bool {
    if (!isset($block_info['theme'])) {
      return FALSE;
    }

    return $this->themeHandler ?
      $this->themeHandler->themeExists($block_info['theme']) :
      FALSE;
  }

  /**
   * Process block data for context blocks.
   *
   * @param string $context_block
   *   The context block ID.
   *
   * @return array
   *   Processed context block data.
   */
  private function getContextBlockData(string $context_block): array {
    if (!$this->contextModuleEnabled) {
      return [];
    }
    return [];
  }

}
