<?php

namespace Drupal\audit_export\Plugin\AuditExport;

use Drupal\audit_export_core\AuditExportPluginBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Plugin implementation of the audit_export.
 *
 * @AuditExport(
 *   id = "site_report",
 *   label = @Translation("Site Report"),
 *   description = @Translation("A general batch of items to check in a report."),
 *   data_type = "process",
 *   identifier = "report_name",
 *   group = "general",
 *   dependencies = {}
 * )
 */
final class AuditExportSiteReport extends AuditExportPluginBase {
  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->configFactory = $container->get('config.factory');
    $instance->moduleHandler = $container->get('module_handler');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $entity_type_manager = NULL,
    ?ConfigFactoryInterface $config_factory = NULL,
    ?ModuleHandlerInterface $module_handler = NULL,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager);
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
    $this->setHeaders(
      [
        'Report',
        'Description',
        'Status',
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function prepareData(): array {
    $reports = [];

    $reports[] = [
      'name' => 'php_version',
      'label' => 'PHP Version',
      'description' => 'Return PHP version that\'s currently installed on the environment.',
      'process' => 'checkPhpVersion',
    ];

    $reports[] = [
      'name' => 'ip_address',
      'label' => 'IP Address',
      'description' => 'Return the IP address of the environment.',
      'process' => 'checkIpAddress',
    ];

    $reports[] = [
      'name' => 'default_theme',
      'label' => 'Default theme',
      'description' => 'The current default theme for the site.',
      'process' => 'checkDefaultTheme',
    ];

    $reports[] = [
      'name' => 'admin_theme',
      'label' => 'Admin theme',
      'description' => 'The current admin theme for the site.',
      'process' => 'checkAdminTheme',
    ];

    $reports[] = [
      'name' => 'ecommerce',
      'label' => 'E-commerce',
      'description' => 'Check installed modules for known ecommerce configuration.',
      'process' => 'checkEcommerce',
    ];

    $reports[] = [
      'name' => 'git_version_control',
      'label' => 'Git version control',
      'description' => 'Check if git version control is detected.',
      'process' => 'checkGitHandle',
    ];

    $reports[] = [
      'name' => 'drupal_version',
      'label' => 'Drupal version',
      'description' => 'The current Drupal version for the site.',
      'process' => 'checkDrupalVersion',
    ];

    return $reports;
  }

  /**
   * {@inheritdoc}
   */
  public function processData(array $params): array {
    $report_data = [];
    if (!empty($params["row_data"]) && !empty($params["row_data"]["process"])) {
      $method = $params["row_data"]["process"];
      if (method_exists($this, $method)) {
        $report_data = call_user_func([$this, $method]);
      }
    }

    return [
      $params["row_data"]["label"],
      $params["row_data"]["description"],
      !empty($report_data['status']) ? $report_data['status'] : NULL,
    ];
  }

  /**
   * Return PHP version for current environment.
   *
   * @return array
   *   An array containing the PHP version.
   */
  // phpcs:ignore
  private function checkPhpVersion(): array {
    return ['status' => PHP_VERSION];
  }

  /**
   * Return the IP address of the environment running the report.
   *
   * @return array
   *   An array containing the IP address.
   */
  // phpcs:ignore
  private function checkIpAddress(): array {
    $possibleEnvVars = ['SERVER_ADDR', 'HOST_IP', 'DOCKER_HOST_IP'];
    $ipAddress = 'Unknown';
    foreach ($possibleEnvVars as $envVar) {
      if (!empty($_SERVER[$envVar])) {
        $ipAddress = $_SERVER[$envVar];
        break;
      }
    }
    return ['status' => $ipAddress];
  }

  /**
   * Return default theme for the site.
   *
   * @return array
   *   An array containing the default theme.
   */
  // phpcs:ignore
  private function checkDefaultTheme(): array {
    if ($this->configFactory) {
      return ['status' => $this->configFactory->get('system.theme')->get('default')];
    }
    return ['status' => 'Unknown'];
  }

  /**
   * Return admin theme for the site.
   *
   * @return array
   *   An array containing the admin theme.
   */
  // phpcs:ignore
  private function checkAdminTheme(): array {
    if ($this->configFactory) {
      return ['status' => $this->configFactory->get('system.theme')->get('admin')];
    }
    return ['status' => 'Unknown'];
  }

  /**
   * Check if ecommerce modules are enabled.
   *
   * @return array
   *   An array indicating if ecommerce modules are detected.
   */
  // phpcs:ignore
  private function checkEcommerce(): array {
    if (!$this->moduleHandler) {
      return ['status' => 'Unknown'];
    }

    $modules = ['commerce', 'ubercart', 'payment', 'pay', 'stripe_pay', 'donate'];
    $enabled = array_filter($modules, function ($module) {
      return $this->moduleHandler->moduleExists($module);
    });

    return ['status' => !empty($enabled) ? 'Enabled' : 'Not detected'];
  }

  /**
   * Detect if git version control is being used.
   *
   * @return array
   *   An array indicating if git is detected.
   */
  // phpcs:ignore
  private function checkGitHandle(): array {
    $max_parent = 4;
    $currentDir = DRUPAL_ROOT;
    for ($i = 0; $i < $max_parent; $i++) {
      if (file_exists($currentDir . '/.git')) {
        return ['status' => 'Git detected'];
      }
      $currentDir = dirname($currentDir);
    }
    return ['status' => 'Git not detected'];
  }

  /**
   * Return the current Drupal version.
   *
   * @return array
   *   An array containing the Drupal version.
   */
  // phpcs:ignore
  private function checkDrupalVersion(): array {
    return ['status' => \Drupal::VERSION];
  }

}
