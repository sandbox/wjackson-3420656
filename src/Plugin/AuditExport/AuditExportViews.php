<?php

namespace Drupal\audit_export\Plugin\AuditExport;

use Drupal\views\Views;
use Drupal\audit_export_core\AuditExportPluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Site\Settings;

/**
 * Plugin implementation of the audit_export for Views.
 *
 * @AuditExport(
 *   id = "views_audit",
 *   label = @Translation("Views Audit"),
 *   description = @Translation("Audit views and their displays, including VBO usage and module source."),
 *   data_type = "process",
 *   identifier = "view",
 *   group = "general",
 *   dependencies = {}
 * )
 */
final class AuditExportViews extends AuditExportPluginBase {
  use StringTranslationTrait;

  /**
   * Build headers for Views Audit report.
   */
  public function __construct() {
    // Define base headers.
    $headers = [
      'View Name',
      'Display Name',
      'Status',
      'Type',
      'Display Path',
      'Module Name',
    ];

    // Add VBO Usage header only if the views_bulk_operations module is enabled.
    if (\Drupal::moduleHandler()->moduleExists('views_bulk_operations')) {
      $headers[] = 'VBO Usage';
    }

    $this->setHeaders($headers);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareData(): array {
    $views_and_displays = [];

    // Ensure the views module is enabled.
    if (\Drupal::moduleHandler()->moduleExists('views')) {
      $views_data = Views::getAllViews();

      foreach ($views_data as $view) {
        $displays = array_keys($view->get('display'));
        // Filter out the default display if other displays exist.
        if (count($displays) > 1 && isset($displays['default'])) {
          unset($displays['default']);
        }

        foreach ($displays as $display_id) {
          $views_and_displays[] = [
            'view_name' => $view->id(),
            'display_id' => $display_id,
          ];
        }
      }
    }

    return $views_and_displays;
  }

  /**
   * {@inheritdoc}
   */
  public function processData(array $params): array {
    $view_name = $params['row_data']['view_name'];
    $display_id = $params['row_data']['display_id'];

    // Load the executable view.
    $view = Views::getView($view_name);
    if (!$view) {
      return [];
    }

    $view->setDisplay($display_id);
    $display = $view->getDisplay();

    // Get display options and check the title.
    $display_options = $display->getOption('display_options');
    $display_name = $display_options['display_title'] ?? $display_id;

    // Get the display path.
    $display_path = $this->getDisplayPath($view, $display_id);

    // Load the view configuration entity to get the label and status.
    $view_config = \Drupal::entityTypeManager()->getStorage('view')->load($view_name);
    $status = $view_config && $view_config->status() ? 'Enabled' : 'Disabled';
    $view_label = $view_config ? $view_config->label() : $view_name;

    $module_name = $this->getModuleName($view_name);
    $type = $this->getViewType($view);

    // Build the base row data.
    $row_data = [
      $view_label . " ($view_name)",
      $display_name,
      $status,
      $type,
      $display_path,
      $module_name,
    ];

    // Add VBO usage data only if the module is enabled.
    if (\Drupal::moduleHandler()->moduleExists('views_bulk_operations')) {
      $vbo_usage = $this->displayUsesVbo($view, $display_id);
      $row_data[] = !empty($vbo_usage) ? implode(', ', $vbo_usage) : NULL;
    }

    return $row_data;
  }

  /**
   * Get the path for a view display.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view object.
   * @param string $display_id
   *   The display ID.
   *
   * @return string
   *   The path for the display, or empty string if no path is found.
   */
  private function getDisplayPath($view, $display_id): string {
    $display_path = $view->getDisplay($display_id);
    if (!$display_path->getOption('path')) {
      return '';
    }

    return $display_path->getOption('path');
  }

  /**
   * Check if the specific display uses Views Bulk Operations (VBO).
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view object loaded with the specific display.
   * @param string $display_id
   *   The display ID.
   *
   * @return array
   *   An array of actions if VBO is used, otherwise empty.
   */
  private function displayUsesVbo($view, $display_id): array {
    $actions = [];
    $all_actions = \Drupal::service('plugin.manager.action')->getDefinitions();

    foreach ($view->getHandlers('field', $display_id) as $item) {
      if (isset($item['table']) && $item['table'] === 'views_bulk_operations') {
        foreach ($item['operations'] as $action_id => $action_details) {
          $action_label = $all_actions[$action_id]['label'] ?? $action_id;
          $actions[] = $action_label;
        }
        return $actions;
      }
    }

    return $actions;
  }

  /**
   * Get the module name based on the view's machine name.
   *
   * @param string $view_name
   *   The view's machine name.
   *
   * @return string
   *   The name of the module or an empty string if not found.
   */
  private function getModuleName(string $view_name): string {
    // Check if the view is provided by a module or optional.
    $module_handler = \Drupal::moduleHandler();
    $module_list = $module_handler->getModuleList();

    foreach ($module_list as $module_name => $module_info) {
      $module_path = $module_info->getPath();

      // Check in config/install directory.
      if ($this->searchConfigDirectory($module_path . '/config/install', $view_name)) {
        return $module_name;
      }

      // Check in config/optional directory.
      if ($this->searchConfigDirectory($module_path . '/config/optional', $view_name)) {
        return $module_name;
      }

      // Check for views.view.VIEWNAME.yml in the module root.
      if (file_exists(DRUPAL_ROOT . '/' . $module_path . '/config/install/views.view.' . $view_name . '.yml')) {
        return $module_name;
      }
    }

    // If not found in module configs, check if it's defined in code.
    foreach ($module_list as $module_name => $module_info) {
      $module_path = $module_info->getPath();
      if ($this->searchModuleFiles($module_path, $view_name)) {
        return $module_name;
      }
    }

    // If module not found, check if it's in the config sync directory.
    $config_sync_directory = Settings::get('config_sync_directory');
    if ($config_sync_directory && $this->searchConfigDirectory($config_sync_directory, $view_name)) {
      // Views in config sync are typically custom.
      return 'custom_module';
    }

    return '';
  }

  /**
   * Search a config directory for view configurations.
   *
   * @param string $directory
   *   The directory to search in.
   * @param string $view_name
   *   The name of the view to search for.
   *
   * @return bool
   *   TRUE if the view configuration is found, FALSE otherwise.
   */
  private function searchConfigDirectory(string $directory, string $view_name): bool {
    if (!is_dir($directory)) {
      return FALSE;
    }

    $view_config_file = $directory . '/views.view.' . $view_name . '.yml';
    return file_exists($view_config_file);
  }

  /**
   * Recursively search module files for view definitions.
   *
   * @param string $module_path
   *   The path to the module directory.
   * @param string $view_name
   *   The name of the view to search for.
   *
   * @return bool
   *   TRUE if the view definition is found, FALSE otherwise.
   */
  private function searchModuleFiles(string $module_path, string $view_name): bool {
    try {
      $directory = new \RecursiveDirectoryIterator(DRUPAL_ROOT . '/' . $module_path);
      $iterator = new \RecursiveIteratorIterator($directory);
      $regex = new \RegexIterator($iterator, '/\.(php|inc)$/i');

      foreach ($regex as $file) {
        // Skip test files.
        if (strpos($file->getPathname(), '/tests/') !== FALSE) {
          continue;
        }

        $contents = file_get_contents($file->getPathname());

        // Search for view definitions in code.
        if (preg_match('/[\'"]' . preg_quote($view_name, '/') . '[\'"]\s*=>\s*\[/', $contents) ||
          preg_match('/->addView\s*\(\s*[\'"]' . preg_quote($view_name, '/') . '[\'"]/', $contents) ||
          preg_match('/views\.view\.' . preg_quote($view_name, '/') . '/', $contents)) {
          return TRUE;
        }
      }
    }
    catch (\Exception $e) {
      \Drupal::logger('audit_export')->error('Error searching module files: @message', ['@message' => $e->getMessage()]);
    }

    return FALSE;
  }

  /**
   * Get the type of the view.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view object.
   *
   * @return string
   *   The type description.
   */
  private function getViewType($view): string {
    // Load the view configuration entity to determine its type.
    $view_config = \Drupal::entityTypeManager()->getStorage('view')->load($view->id());

    // Check if view config entity, implying it's stored in the database.
    if ($view_config && $view_config->isNew() === FALSE) {
      return $this->t('Database');
    }
    else {
      return $this->t('In Code');
    }
  }

}
