<?php

namespace Drupal\audit_export\Plugin\AuditExport;

use Drupal\audit_export_core\AuditExportPluginBase;
use Drupal\user\Entity\Role;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Plugin implementation of the AuditExport for user roles and permissions.
 *
 * @AuditExport(
 *   id = "active_users_roles_audit",
 *   label = @Translation("Active Users and Roles Audit"),
 *   description = @Translation("Active users, roles, and permissions."),
 *   group = "users",
 *   identifier = "source_module",
 *   data_type = "cross",
 *   dependencies = {},
 * )
 */
final class AuditExportActiveUsersRoles extends AuditExportPluginBase {
  use StringTranslationTrait;

  /**
   * Constructor for AuditExportActiveUsersRoles class.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    // Set the headers to the role labels.
    $this->setHeaders($this->buildUserRoleHeaders());
    // Set cross-tab header label to "Module" for first column.
    $this->setCrossTabHeaderLabel($this->t('Module'));
  }

  /**
   * {@inheritdoc}
   */
  public function prepareData(): array {
    return $this->getPermissionModules();
  }

  /**
   * {@inheritdoc}
   */
  public function processData(array $params): array {
    // Fetch the module information from the provided row data.
    $module = $params['row_data'];

    // Use 'administrator' or the first available role to determine header.
    $roles = Role::loadMultiple();
    $header_role_id = isset($roles['administrator']) ? 'administrator' : key($roles);

    // Initialize the output row with the module header.
    $output = [$this->permissionAudit($header_role_id, $module)['module_header']];

    // Process all roles.
    $role_data = [];
    foreach ($roles as $rid => $role) {
      // Fetch permission data for the current role and module.
      $audit_result = $this->permissionAudit(
        $rid, [
          'source_module' => $module['source_module'],
          'permissions' => $module['permissions'] ?? [],
        ]
      );

      // Append the audit results to the role data array if valid.
      if ($audit_result && !isset($audit_result['error'])) {
        $role_data[] = $audit_result;
      }
      else {
        // Log if the audit result was empty or missing permissions.
        \Drupal::logger('audit_export')->notice(
          'No permissions found for role: @role in module: @module', [
            '@role' => $role->label(),
            '@module' => $module['source_module'],
          ]
        );
      }
    }

    // Get all permissions grouped by their provider.
    $permissions = \Drupal::service('user.permissions')->getPermissions();
    $permissions_by_provider = [];

    foreach ($permissions as $key => $permission) {
      if (isset($permission['provider'])) {
        $permissions_by_provider[$permission['provider']][] = $key;
      }
    }

    // Check if permissions exist for the current module.
    if (!isset($permissions_by_provider[$module['source_module']])) {
      \Drupal::logger('audit_export')->notice(
        'No permissions found for module: @module', [
          '@module' => $module['source_module'],
        ]
      );
      $permissions_by_provider[$module['source_module']] = [];
    }

    // Process the gathered role data to determine permission coverage.
    foreach ($role_data as $role_datum) {
      // For administrator role, always show "All permissions".
      if (isset($role_datum['is_admin']) && $role_datum['is_admin']) {
        $output[] = $this->t('All permissions');
      }
      // Check if the role data contains permissions.
      elseif (!empty($role_datum) && !empty($role_datum["permissions"])) {
        // Check permission counts to determine if all permissions are granted.
        if (count($permissions_by_provider[$module['source_module']]) === count($role_datum["permissions"])) {
          $output[] = $this->t('All permissions');
        }
        else {
          // Clean permissions, ensuring they're all strings.
          $safe_permissions = array_map(function ($p) {
            return is_string($p) ? $p : (
            is_object($p) && method_exists($p, '__toString') ? (string) $p : ''
            );
          }, $role_datum["permissions"]);

          // Clean and format the list of permissions for display.
          $output[] = $this->permissionListClean(implode(', ', $safe_permissions));
        }
      }
      else {
        // If no permissions were found role, append 'None' to the output.
        $output[] = $this->t('None');
      }
    }

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareCrossTabScripts(): array {
    // Build a roles array in the expected format.
    $roles_array = [];
    $roles = Role::loadMultiple();
    foreach ($roles as $rid => $role) {
      $roles_array[$rid] = $role->label();
    }

    // Return the cross-tab script configuration that matches the D7 format.
    return [
      'user_role_count' => [
        'label' => $this->t('# of users'),
        'name' => 'user_role_count',
        'method' => 'userRoleCount',
        'roles' => ['roles' => $roles_array],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function processDataPreProcess(): array {
    return [[
      'name' => 'user_role_perms_data_heading',
      'method' => 'modulePermsDataHeading',
    ],
    ];
  }

  /**
   * Generate module permission data heading.
   *
   * @return array
   *   Module permission data heading.
   */
  public function modulePermsDataHeading(): array {
    // Create a heading row with "Module" and "Permissions".
    $data = [$this->t('Module'), $this->t('Permissions')];

    // Pad the array to match the number of roles.
    return array_pad($data, count(Role::loadMultiple()) + 1, '');
  }

  /**
   * Clean the permission list by aggregating them into human-readable format.
   *
   * @param string $permission_item
   *   A string containing various permissions.
   *
   * @return string
   *   A formatted string of permissions.
   */
  private function permissionListClean(string $permission_item): string {
    // First, strip all HTML tags.
    $permission_item = strip_tags($permission_item);

    // Remove Drupal placeholder markers.
    $permission_item = preg_replace('/<em class="placeholder">([^<]+)<\/em>/', '$1', $permission_item);

    // Replace HTML entities with their actual characters.
    $permission_item = html_entity_decode($permission_item, ENT_QUOTES);

    // Now proceed with the aggregation logic.
    $patterns = [
      '/Execute (.+)/',
      '/(.+): Create new content/',
      '/(.+): Edit own content/',
      '/(.+): Delete own content/',
      '/(.+): View any content/',
    ];

    $aggregatedPermissions = [];
    $unmatchedItems = [];

    $permission_items = explode(', ', $permission_item);

    foreach ($permission_items as $item) {
      $matched = FALSE;

      foreach ($patterns as $pattern) {
        if (preg_match($pattern, $item, $matches)) {
          $matched = TRUE;
          $action = str_replace($matches[1], '%slug%', $item);
          $slug = $matches[1];
          $aggregatedPermissions[$action][] = $slug;
          break;
        }
      }

      if (!$matched) {
        $unmatchedItems[] = $item;
      }
    }

    $output = [];

    foreach ($aggregatedPermissions as $pattern => $slugs) {
      $action = str_replace('%slug%', '', $pattern);
      $output[] = trim($action, ': ') . ': ' . implode(', ', array_unique($slugs));
    }

    $output = array_merge($output, $unmatchedItems);

    return implode('; ', $output);
  }

  /**
   * Perform permission audit for the specified role and module.
   *
   * @param string $rid
   *   The role ID.
   * @param array|null $module
   *   The module information.
   *
   * @return array
   *   The audit results.
   */
  private function permissionAudit(string $rid, ?array $module = NULL): array {
    // Load the role by its ID.
    $role = Role::load($rid);
    if (!$role) {
      return ['error' => 'Invalid role ID provided.'];
    }

    // Initialize permission data and audit arrays.
    $permission_data = [];
    $audit = [];

    // Get module name - handle various types.
    $module_provider = $module['source_module'] ?? '';
    $module_name = '';
    try {
      if (!empty($module_provider)) {
        $module_name = (string) \Drupal::service('module_handler')->getName($module_provider);
      }
    }
    catch (\Exception $e) {
      $module_name = $module_provider;
    }

    // Check for administrator role - if so, return "All permissions".
    $admin_role_id = $this->getAdministratorRoleId();
    if ($admin_role_id === $rid) {
      // For admin role, return a special result indicating all permissions.
      return [
        'module_header' => $module_name,
        'permissions' => ['All permissions'],
        'is_admin' => TRUE,
      ];
    }

    // Get the permissions assigned to this role.
    $role_permissions = $role->getPermissions();

    // Get all permissions using the PermissionHandler service.
    $permissions = \Drupal::service('user.permissions')->getPermissions();

    // Filter permissions by the module provider.
    $module_permissions = array_filter(
      $permissions,
      function ($permission) use ($module) {
        return isset($permission['provider']) && $permission['provider'] === $module['source_module'];
      }
    );

    // Iterate over module permissions and check against role permissions.
    foreach ($module_permissions as $perm => $perm_item) {
      // Check if the permission is granted to the role.
      if (in_array($perm, $role_permissions)) {
        // Extract title as a simple string, avoiding strip_tags entirely.
        $title = '';
        if (isset($perm_item['title'])) {
          if (is_string($perm_item['title'])) {
            $title = $perm_item['title'];
          }
          elseif (is_object($perm_item['title']) && method_exists($perm_item['title'], '__toString')) {
            $title = (string) $perm_item['title'];
          }
          elseif (is_array($perm_item['title']) && isset($perm_item['title']['#markup'])) {
            $title = is_string($perm_item['title']['#markup']) ?
              $perm_item['title']['#markup'] :
              (is_object($perm_item['title']['#markup']) && method_exists($perm_item['title']['#markup'], '__toString') ?
                (string) $perm_item['title']['#markup'] : 'Unknown permission');
          }
          else {
            $title = "Permission from {$module['source_module']}";
          }
        }
        else {
          $title = $perm;
        }

        // For description, similar handling but simpler.
        $description = '';
        if (isset($perm_item['description'])) {
          if (is_string($perm_item['description'])) {
            $description = $perm_item['description'];
          }
          elseif (is_object($perm_item['description']) && method_exists($perm_item['description'], '__toString')) {
            $description = (string) $perm_item['description'];
          }
          else {
            $description = '';
          }
        }

        $permission_data[$module['source_module']]['permissions'][$perm] = [
          'title' => $title,
          'description' => $description,
          'granted' => TRUE,
        ];
      }
    }

    // Set the display name of the module for the header.
    if (!empty($permission_data[$module['source_module']])) {
      $permission_data[$module['source_module']]['display_name'] = $module_name;
    }

    // Prepare the audit result.
    $audit['module_header'] = $module_name ?: $module_provider;
    $audit['permissions'] = !empty($permission_data[$module['source_module']]['permissions'])
      ? array_column($permission_data[$module['source_module']]['permissions'], 'title')
      : [];

    return $audit;
  }

  /**
   * Get the administrator role ID.
   *
   * @return string|null
   *   The administrator role ID or null if not found.
   */
  private function getAdministratorRoleId(): ?string {
    // Query for roles with is_admin set to TRUE.
    $admin_roles = \Drupal::entityTypeManager()
      ->getStorage('user_role')
      ->getQuery()
      ->condition('is_admin', TRUE)
      ->execute();

    // Return the first admin role ID found or null if none.
    return !empty($admin_roles) ? reset($admin_roles) : NULL;
  }

  /**
   * Retrieve all permission modules and their permissions.
   *
   * @return array
   *   An array of modules with permissions.
   */
  private function getPermissionModules(): array {
    $permissions_array = [];

    // Load all permissions using the permission handler service.
    $permission_handler = \Drupal::service('user.permissions');
    $all_permissions = $permission_handler->getPermissions();

    // Organize permissions by module.
    foreach ($all_permissions as $permission => $details) {
      $module_name = $details['provider'] ?? 'Unknown';

      // Organize permissions under the source module.
      $permissions_array[$module_name]['source_module'] = $module_name;
      $permissions_array[$module_name]['permissions'][$permission] = [
        'title' => $details['title'] ?? $permission,
        'description' => $details['description'] ?? '',
      ];
    }

    return $permissions_array;
  }

  /**
   * Return an array of role names to be used for header values of the report.
   *
   * @return array
   *   An array of role names.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildUserRoleHeaders(): array {
    $user_roles = [];

    // Use dependency injection when available.
    if ($this->entityTypeManager) {
      $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
      foreach ($roles as $role) {
        $user_roles[] = $role->label();
      }
    }
    else {
      // Fallback to static method if entity type manager is not available.
      // This is not recommended and should be avoided.
      $roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
      foreach ($roles as $role) {
        $user_roles[] = $role->label();
      }
    }

    return $user_roles;
  }

  /**
   * Return the count of users for each role.
   *
   * @param array $roles
   *   The roles to count users for.
   *
   * @return array
   *   An array of user counts for each role.
   */
  public function userRoleCount(array $roles): array {
    // Start with the row label in the first column.
    $row_data = [$this->t('User Role Count')];

    // Make sure we have the roles in the expected format.
    $role_data = $roles["roles"]["roles"] ?? [];

    // Loop through each role and get the count of active users with that role.
    foreach ($role_data as $rid => $role_label) {
      // Query the database to get the count of users with this role.
      try {
        // For anonymous users (no role assignment needed)
        if ($rid === 'anonymous') {
          $count = \Drupal::database()->select('users_field_data', 'ufd')
            ->condition('ufd.uid', 0)
            ->countQuery()
            ->execute()
            ->fetchField();
        }
        // For authenticated users (all users except anonymous)
        elseif ($rid === 'authenticated') {
          $count = \Drupal::database()->select('users_field_data', 'ufd')
            ->condition('ufd.uid', 0, '>')
            ->condition('ufd.status', 1)
            ->countQuery()
            ->execute()
            ->fetchField();
        }
        // For all other roles.
        else {
          // Check which table exists.
          $schema = \Drupal::database()->schema();
          $table_name = '';

          if ($schema->tableExists('user__roles')) {
            // Drupal 11 table.
            $table_name = 'user__roles';
            $field_name = 'roles_target_id';
          }
          elseif ($schema->tableExists('users_roles')) {
            // Drupal 7/8/9 table.
            $table_name = 'users_roles';
            $field_name = 'rid';
          }

          if (!empty($table_name)) {
            $query = \Drupal::database()->select($table_name, 'ur');
            $query->join('users_field_data', 'ufd', 'ur.entity_id = ufd.uid');
            $query->condition("ur.$field_name", $rid);
            $query->condition('ufd.status', 1);
            $count = $query->countQuery()
              ->execute()
              ->fetchField();
          }
          else {
            // If table not found, just return 0.
            $count = 0;
          }
        }
      }
      catch (\Exception $e) {
        \Drupal::logger('audit_export')->error('Error counting users for role @role: @message', [
          '@role' => $rid,
          '@message' => $e->getMessage(),
        ]);
        $count = 0;
      }

      // Format the count and add it to the row.
      $row_data[] = number_format($count);
    }

    return $row_data;
  }

}
