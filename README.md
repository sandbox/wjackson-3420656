# Audit Export

Audit Export is a powerful auditing module for Drupal 11, designed to help site
administrators and developers analyze and report on the entity structure and
overall usage of their Drupal site. It allows for detailed audits, reports,
and exporting capabilities, making it easier to monitor and maintain site
health over time.

## Features

- **Run Audits on Entity Structure and Site Usage**: Audit Export provides
a comprehensive set of audits to determine the structure of content entities,
user roles, blocks, menus, taxonomy, views, and more. Easily keep track of
what's happening on your site and how various entities are being used.

- **Automated Reports via Cron**: Set up reports to run automatically through
Drupal's cron system. Ensure your audit data is always up to date and
accessible when you need it.

- **Export Reports in CSV Format**: Download and export reports in CSV format
to use in external reporting tools or for further analysis in custom workflows.
Export functionality allows for seamless integration with custom data
pipelines.

- **Drush Integration**: Run audits and generate reports directly from the
command line using Drush. This allows for easy automation and integration
into CI/CD workflows.

- **Custom Plugin Architecture**: Build and integrate your own custom audit
reports by extending the Audit Export plugin system. Whether you need to audit
custom data or build reports tailored to your unique use cases, the plugin
architecture makes it easy to extend the module's functionality.

- **Remote Posting for Advanced Use Cases**: Optionally configure the module
to remotely post audit data to external endpoints for more advanced use cases.
This can be useful for integrating with external monitoring systems or
third-party applications that need to consume audit data.

## Use Cases

- **Entity Structure Audits**: Understand how content types, user roles,
taxonomy vocabularies, and other entities are used across your site.
Identify unused or poorly configured entities and optimize your site's
structure.

- **Automated Reporting**: Schedule automated reports to run via cron,
ensuring that you always have the latest audit data available for review.
Stay informed about changes and trends in site usage.

- **Custom Reports**: Export audit data in CSV format to easily integrate with
third-party tools, such as Google Sheets, Excel, or custom-built analytics
platforms.

- **Custom Plugin Development**: Build your own audits by leveraging the Audit
Export plugin system. Whether auditing custom entities, third-party modules,
or specific configurations, Audit Export gives you the flexibility to create
the reports that matter most to your organization.

- **Advanced Monitoring**: Use the remote posting feature to send audit data to
external systems for further processing, monitoring, or analysis. Keep track of
site health across multiple environments by pushing reports to central
monitoring systems.

## Installation

1. Download and install the module in your Drupal 11 site. You can do this by
placing the module in the `/modules/custom` directory or installing it through
Composer.

    ```bash
    composer require drupal/audit_export
    ```

2. Enable the module:

    ```bash
    drush en audit_export
    ```

3. Configure the module by navigating to the admin interface at
`/admin/config/audit_export`.

## Usage

### Running Audits via Drush

You can run audits directly via Drush commands. This is useful for
automating reports in scripts or running ad-hoc audits from the command line.

```bash
drush audit-export:run
```

### Exporting Reports

Once audits have been run, you can export the results as CSV files from the
admin interface or directly via Drush commands.

```bash
drush audit-export:export --format=csv
```

### Setting up Cron
To automate report generation, set up cron jobs in your Drupal site to run
audits periodically. Visit the cron settings page to configure when these jobs
run.

### Creating Custom Audit Plugins
_Coming Soon_

### License
This project is licensed under the GNU General Public License, version 2.

### Maintainers
Will Jackson - Module Maintainer
Contributions are welcome! Please submit issues and pull requests through
the GitHub repository.
